######
"""



On the other hand, the angle between

Ki = array([4.01513195, 0.        , 0.        ])

Kdetector = array([3.64006633, 1.41115784, 0.93799533])

is just 24.96 deg.

Using G in the lab frame, I get:

K1 = Ki+G_lab = array([ 3.01513195, -1.        , -1.        ])

then the angle between Ki and K1 is the Bragg angle as it should be...


Hi Alessandro,


using my definitions as in the xrs_utilities.cixsUBgetQ_primo(tthv, tthh, psi) function:

the G=[-1,-1,-1] vector in the Lab-frame is after rotation by the Bragg angle around the lab-y-axis and after rotation around the G-vecotor:

U.dot(B).dot(G) = [ 1.42476636,  1.00415622, -0.98849196]

K_in in the lab frame is:

K_in = [4.01513195, 0.        , 0.        ]

And the vector to the detector (in the lab frame) is

KO = 2.0*np.pi/lambdao*vrot(vrot(X,Y,-tthv) ,Z, tthh) = [3.64006633, 1.41115784, 0.93799533] (if X in the lab is positive, tthv rotates negative)

so the angle between G_lab and KO is: 45.13 degrees (then the asymmetry should be subtracted from this, right?)

The surface normal before any crystal rotations is (in the crystal frame):

n = [-0.9014985 , -1.11187038, -0.97521521]

in the lab frame this becomes:

U.dot(B).dot(n) = [ 1.53082821,  0.86861388, -0.95785185]

after rotating this by psi:

[ 1.44878323, -0.46741813,  0.8261643 ]


Now I am very confused about the vector called "v" in the function:

v = np.array([-np.sin(np.radians(th)), 0.0, np.cos(np.radians(th))])

If this was the G vector after rotation of the Bragg angle in the lab-frame it should be +np.sin(np.radians(th)). But I doubt this was meant there... it may again have to do with the definitions of the lab X and instrument X-directions (because K_in is positive in X!!!)???


does any of this make any sense?

cheers

Christoph




produrre ancora grafico coefficienti per fit nominale cercando parametro slit e magnification

mostrare per varie scelte coefficienti curve componenti  .
Confrontare normalizzazioni.
Confronto con teoria.


Usare curve diagonali . COntrollare normalizzazione. 
Produrre fuori-diagonale. Purificare. COntrollare coefficienti.
Cercare di avere i buoni valori agli estremi

"""


import sys
from XRStools import resintesi

import matplotlib
matplotlib.use('Qt4Agg')

# from silx import sx
from silx.gui.plot import Plot2D
from silx.gui import qt
qapp = qt.QApplication([])

    
from XRStools import xrs_read, theory, xrs_extraction, xrs_rois, roifinder_and_gui, xrs_utilities, math_functions, roiSelectionWidget, Bragg
from pylab import *

# ion()
import numpy as np
from scipy import interpolate, signal, integrate, constants, optimize, ndimage
from XRStools import ixs_offDiagonal,xrs_utilities

import h5py

def saveDictH5( obj, fn):
    file = h5py.File(fn,"w")
    for k in obj.keys():
        file[k] = obj[k]
    file.close()
def loadDictH5(  fn):
    obj = {}
    file = h5py.File(fn,"r")
    for k in file:
        obj[k] = file[k] [:]
    file.close()
    return obj

kapmonitor = "kaprixs"
kapmonitor = "kap1dio"

##############################################################################
# off-focus resolution tests
##############################################################################

repertorio = "/data/id20/inhouse/data/run5_15/run6_hc2251"
# repertorio = "/olddisk/data/id20/inhouse/data/run5_15/run6_hc2251"

############
# off-dia terzo: G=[-1,-1,-1], q0=[1.0, 1.0, -0.5], q1=[0.0, 0.0, -1.5]
############

#
#
# the tthh angle was POS. 21.18 degrees, which is the
# correct angle for the primo crystal!!!!!!!!!!!!!!
#
# BUT terzo crystal was used !!!!!!!!!!!!!!!!!!!!!
#
# this point may be not valid !!!!!!!!!!!!!!!!!!!!
#

from XRStools import ixs_offDiagonal

asymm_grad       =  5.0   
tthv_grad        =      13.51
tth_grad         =  21.19


psi_grad         = 25.4  # 65.23

source_distance = 60.0
magnification = +0.128   ### nominal a 0.118  poi 0.1, 0.11, 0.12, 0.13, 0.14
E0 = 7950.0
hkl =  np.array(  [ 1,1,1  ])
DeltaAngle =0.00000*180/np.pi   ### if the two crystal of the monocromator are parallel this parameter is Zero

Ew_source_ev      = 4.0
Aw_rocking_rad   = 2.0e-3
Hw_slit_mm        = 0.4
nEpoints = 500
nApoints = 1000
nHpoints = 100

if(0):

    #############################################################################################################################"
    ## This routine perform the calculation of the averaged coefficients considering different wavelenghts
    ## and different incoming directions.
    ## Contribution from different energies are considered beaing mutually incoherent.
    ## Moreover ech incoming direction is considered incoherent.
    ## This is a strong assumption because the beam in reality has a coherence lenght.
    ## But here we calculate the weight of all the contributions by a sort of ray-tracing, the weight being obtained
    ## from dynamical theory for a plane wave of a given energy and of a given direction calculating the reflection
    ## of the monochromators and what happens in the sample:
    ## A sampling is done over the energy, considering and energy width Ew_source_ev around nominal E0.
    ## A sampling is done also over the wavefront height. Each height define and incoming angle
    ## ( given by the source position and the distance from it).


    ## E0 is the nominal energy, eV
    ## source_distance is given in meters
    
    STW_infos =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl,  asymm_grad = asymm_grad,
                                              tthv_grad=tthv_grad, tth_grad=tth_grad,
                                              source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle,
                                              Ew_source_ev    = Ew_source_ev,  
                                              Aw_rocking_rad = Aw_rocking_rad, 
                                              Hw_slit_mm = Hw_slit_mm,
                                              nEpoints = nEpoints, nApoints= nApoints, nHpoints = nHpoints
    )
    
    np.savetxt("monochromator_response_energy.txt",monochromator_response_energy)
    
    np.savetxt("rocking_curve.txt",rocking_curve.T)

    raise



roifinder = roifinder_and_gui.roi_finder()
print(" REPERTORIO ", repertorio)
offdia_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column=kapmonitor)






#######################################################################
## ACTIVATE THIS ONCE TO GENERATE THE ROIS FOR BACKGROUND EXTRACTION
##
if 0:
    image4roi =  offdia_ixsobj.SumDirect( [2934] )
    w4r = roiSelectionWidget.launch4MatPlotLib(im4roi=image4roi, layout = "1X5-1" )
    
    if w4r.isOK :
        roiobj = w4r.getRoiObj()
        roiobj.writeH5('back_ROI_Si111_q0_10_10_-05.h5') 
    sys.exit(0)

#######################################################################
## ACTIVATE THIS ONCE TO GENERATE THE ROIS FOR SIGNAL EXTRACTION
##
roiName = "ROI_Si111_q0_10_10_-05.h5"
if 0:
    image4roi =  offdia_ixsobj.SumDirect( [2934] )
    w4r = roiSelectionWidget.launch4MatPlotLib(im4roi=image4roi, layout = "1X5-1" )
    
    if w4r.isOK :
        roiobj = w4r.getRoiObj()
        roiobj.writeH5( roiName ) 
    sys.exit(0)




    

######################################################
### ROI IS SET HERE FOR OFFDIAGONAL DATA
###
print(" LOADING ROI for offdiagonal and diagonal")
roifinder.roi_obj.loadH5(roiName)    
offdia_ixsobj.set_roiObj(roifinder.roi_obj)


## LONG ROCKING CURVE FIT
## And generation of oefficients
## Once you have generated on or more STW files  you can disactivate this section. STW files can be choosed and reused, while skipping this section. 
if(1):
    longrockingcurve_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column=kapmonitor)
    longrockingcurve_ixsobj.set_roiObj(roifinder.roi_obj)
    #################################################################
    ##### ACTIVATE THIS ONCE TO LOAD THE LONG ROCKING CURVE
    if(0):
        longrockingcurve_ixsobj.loadRockingCurve(list(range(2936,3457,4)),direct=True, storeInsets = True)
        longrockingcurve_ixsobj.save_state_hdf5(  "longrockingcurve_ixsobj.h5", "after_loading", comment="" , overwriteFile = True,  overwriteGroup=True)
        sys.exit(0)

    if(1):
        # this reloads the rocking curve
        longrockingcurve_ixsobj.load_state_hdf5(  "longrockingcurve_ixsobj.h5", "after_loading")
        ## alignes using the R(ocking)C(urve) monitor alirixs
        longrockingcurve_ixsobj.stitchRockingCurves(I0moni=kapmonitor,RCmoni='alirixs')
        ## save it for future inspection
        longrockingcurve_ixsobj.save_state_hdf5(  "longrockingcurve_ixsobj.h5", "after_stichting_aligning", overwriteFile = False)
        
        #####################################################################"
        #### You find  alot of documentations in the source of this routine
        STW_infos =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl, hkl_mono=hkl,  asymm_grad = asymm_grad,
                                                  tthv_grad=tthv_grad, tth_grad=tth_grad,psi_grad = psi_grad,
                                                  source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle,
                                                  Ew_source_ev    = Ew_source_ev,  
                                                  Aw_rocking_rad = Aw_rocking_rad, 
                                                  Hw_slit_mm = Hw_slit_mm,
                                                  nEpoints = nEpoints, nApoints= nApoints, nHpoints = nHpoints
        )


        saveDictH5( STW_infos,"STW_infos111_nominal_%f.h5"%magnification) 
    
        np.savetxt ("rocking_curve111_nominal_%f.txt"%magnification,STW_infos["RC_curve"].T)
        np.savetxt       ("A_curve111_nominal_%f.txt"%magnification,STW_infos["A_curve"].T)
        np.savetxt       ("B_curve111_nominal_%f.txt"%magnification,STW_infos["B_curve"].T)
        np.savetxt       ("C_curve111_nominal_%f.txt"%magnification,STW_infos["C_curve"].T)


        
        STW_infos_nowidth =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl, hkl_mono=hkl,  asymm_grad = asymm_grad,
                                                          tthv_grad=tthv_grad, tth_grad=tth_grad,psi_grad = psi_grad,
                                                          source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle,
                                                          Ew_source_ev    = Ew_source_ev,  
                                                          Aw_rocking_rad = Aw_rocking_rad, 
                                                          Hw_slit_mm = Hw_slit_mm*0.00001,
                                                          nEpoints = nEpoints, nApoints= nApoints, nHpoints = nHpoints
        )

        saveDictH5( STW_infos_nowidth,"STW_infos111_all_parallel.h5") 

        
        np.savetxt ("rocking_curve111_allparallel.txt",STW_infos_nowidth["RC_curve"].T)
        np.savetxt       ("A_curve111_allparallel.txt",STW_infos_nowidth["A_curve"].T)
        np.savetxt       ("B_curve111_allparallel.txt",STW_infos_nowidth["B_curve"].T)
        np.savetxt       ("C_curve111_allparallel.txt",STW_infos_nowidth["C_curve"].T)

        stw_2use_for_resynt = STW_infos_nowidth

        
        longrockingcurve_ixsobj.offDiaDataSets[0].alignRCmonitor( stw_2use_for_resynt["RC_curve"]  )
        
        synth_coeffs= resintesi.resintetizzaTV(  longrockingcurve_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0,   
                                                 longrockingcurve_ixsobj.offDiaDataSets[0].alignedRCmonitor.mean(axis=0),
                                                 stw_2use_for_resynt["RC_curve"] , 0.001 )
                                                 ## STW_infos_nowidth["RC_curve"] , 0.001 )
        
        np.savetxt("synt_coeffs111.txt", np.array( [   longrockingcurve_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0 ,
                                                       synth_coeffs]).T)


        np.savetxt("xy111_long.txt", np.array( [   longrockingcurve_ixsobj.offDiaDataSets[0].masterRCmotor   * np.pi/180.0 ,
                                              longrockingcurve_ixsobj.offDiaDataSets[0].alignedRCmonitor.mean(axis=0)]).T  )


        xrads, synth_coeffs_vals =      longrockingcurve_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0      , synth_coeffs


        ## commentare qui sotto  se si vuole tenere ultimo con magnification
        STW_infos    = resintesi.get_resynth(stw_2use_for_resynt  , synth_coeffs_vals,  xrads )
        saveDictH5( STW_infos,"STW_infos111_resynt.h5") 

        np.savetxt(     "rocking_curve111_long_resynt.txt",STW_infos["RC_curve"].T)
        np.savetxt("A_curve111_long_resynt.txt",STW_infos["A_curve"].T)
        np.savetxt("B_curve111_long_resynt.txt",STW_infos["B_curve"].T)
        np.savetxt("C_curve111_long_resynt.txt",STW_infos["C_curve"].T)
        
    sys.exit(0)

#########################################################################
# CHOICE OF THE COEFFICIENTS
#
# STW_infos    = loadDictH5( "STW_infos111_nominal_%f.h5"%magnification) 

STW_infos    = loadDictH5( "STW_infos111_resynt.h5" )
#########################################################################


    
## OFFDIAGONAL
if(0):
    print(" LOADING OFFDIAGONAL FROM RAW DATA")
    offdia_ixsobj.loadRockingCurve(list(range(2937,3458,4)),direct=True, storeInsets = True)
    offdia_ixsobj.save_state_hdf5 (  "offdia_ixsobj_bis.h5", "after_loading", comment="" , overwriteFile = True,  overwriteGroup=True)
    sys.exit(0)
else:
    if(0):
        print(" RE-LOADING OFFDIAGONAL FROM ALREADY EXTRACTED DATA AND REALIGNEMENT.  STW_infos[\"RC_curve\"] IS USED FOR ALIGNEMENT OF THE EXP RC ")

        offdia_ixsobj.load_state_hdf5(  "offdia_ixsobj_bis.h5", "after_loading",)
        
        offdia_ixsobj.stitchRockingCurves(I0moni=kapmonitor,RCmoni='alirixs')

        for k in range(3):
            (pos, M) =  roifinder.roi_obj.red_rois["ROI0%d"%(k+1)]    
            offdia_ixsobj.offDiaDataSets[k].signalMatrix *=  1.0/ (np.less(0,M).sum())

        ###################################################################################
        ############################# THE POINT 17,7 HAS A PROBLEM
        for k in range(3):
            sm = offdia_ixsobj.offDiaDataSets[k].signalMatrix
            sm[17,7] = 0.5*( sm[17,6]+sm[17,8]) 

        #############################
        #################################################""

        for k in range(3):
            offdia_ixsobj.offDiaDataSets[k].normalizeRC()
        
        for k in range(3):
            offdia_ixsobj.offDiaDataSets[k].alignRCmonitor( STW_infos["RC_curve"]  )        
        
        offdia_ixsobj.save_state_hdf5(  "offdia_ixsobj_aligned.h5", "after_stichting_aligning", overwriteFile = False,  overwriteGroup=True)

        np.savetxt("xy111_short_mean.txt", np.array( [   offdia_ixsobj.offDiaDataSets[0].masterRCmotor   * np.pi/180.0 ,
                                              offdia_ixsobj.offDiaDataSets[0].alignedRCmonitor.mean(axis=0)]).T 
        )                       
        
        sys.exit(0)

    else:
        print( " RELOADING OFFDIAG " )
        offdia_ixsobj.load_state_hdf5(  "offdia_ixsobj_aligned.h5", "after_stichting_aligning")

        
# if 1:
#     # problem with data point 32:
#     for ii in list(range(21)):
#         offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[32,ii] = np.mean( (offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[31,ii],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[33,ii])  )
#     # problem with data point 80:
#     for ii in list(range(21)):
#         offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[80,ii] = np.mean( (offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[79,ii],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[81,ii])  )

##############
# diagonal
##############
dia_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column=kapmonitor)
dia_ixsobj.set_roiObj(roifinder.roi_obj)


# plasmon and core part
if 0:

    print("LOADING DIAGONAL")
    dia_ixsobj.loadRockingCurve(list(range(2939,3460,4)), direct=True)
    
        
    print("STITCHING DIAGONAL")
    
    dia_ixsobj.stitchRockingCurves(I0moni=kapmonitor,RCmoni='alirixs')


    for k in range(3):
        (pos, M) =  roifinder.roi_obj.red_rois["ROI0%d"%(k+1)]    
        dia_ixsobj.offDiaDataSets[k].signalMatrix *=  1.0/ (np.less(0,M).sum())
        
    dia_ixsobj.save_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting")
        
    for k in range(3):
        dia_ixsobj.offDiaDataSets[k].normalizeRC()
        dia_ixsobj.offDiaDataSets[k].alignRCmonitor(None)
        
    dia_ixsobj.save_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting_aligning")
    sys.exit(0)
else:
    print( " RE-LOADING DIAG " )
    dia_ixsobj.load_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting_aligning")


# if 1:
#     # problem with data point 32:
#     for ii in list(range( dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix.shape[1]  )):
#         dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[32,ii] = np.mean( (dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[31,ii],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[33,ii])  )
#     # problem with data point 80:
#     for ii in list(range(  dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix.shape[1]   )):
#         dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[80,ii] = np.mean( (dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[79,ii],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[81,ii])  )


#############################
# background for off-diagonal
#############################
back_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column=kapmonitor)
backroifinder = roifinder_and_gui.roi_finder()
backroifinder.roi_obj.loadH5('back_ROI_Si111_q0_10_10_-05.h5')

back_ixsobj.set_roiObj(backroifinder.roi_obj)
# plasmon and core part

if 0 :

    if 0:
        print(" Loading background")
        back_ixsobj.loadRockingCurve(list(range(2937,3458,4)), direct=True)        
        back_ixsobj.save_state_hdf5(  "back_ixsobj.h5", "after_loading", comment="" , overwriteFile = True,  overwriteGroup=True)
        sys.exit(0)
    
    else:
        print(" RE-Loading background")
        back_ixsobj.load_state_hdf5(  "back_ixsobj.h5", "after_loading",)
    
    back_ixsobj.stitchRockingCurves(I0moni=kapmonitor,RCmoni='alirixs')


    ###################################################################################
    ############################# THE POINT 17,7 HAS A PROBLEM
    for k in range(3):
        sm = back_ixsobj.offDiaDataSets[k].signalMatrix
        sm[17,7] = 0.5*( sm[17,6]+sm[17,8]) 
    #############################
    #################################################""

    
    for k in range(3):
        (pos, M) =  backroifinder.roi_obj.red_rois["ROI0%d"%(k+1)]    
        back_ixsobj.offDiaDataSets[k].signalMatrix *=  1.0/ (np.less(0,M).sum())

    for k in range(3):
        back_ixsobj.offDiaDataSets[k].normalizeRC()
        back_ixsobj.offDiaDataSets[k].alignRCmonitor(STW_infos["RC_curve"])

    back_ixsobj.save_state_hdf5(  "back_ixsobj_aligned.h5", "after_stichting_aligning")
    
    # replace background signals by constant fit through signals
    for k in range(3):
        back_ixsobj.offDiaDataSets[k].replaceSignalByConstant([7.95,10.0])

    back_ixsobj.save_state_hdf5(  "back_ixsobj_aligned_rep.h5", "after_stichting_aligning_replacing")

    sys.exit(0)
    
else:
    print( " RELOADING BACK " )
    back_ixsobj.load_state_hdf5(  "back_ixsobj_aligned_rep.h5", "after_stichting_aligning_replacing")
    

if(0):
    print( " checking out what is going on with the background subtraction")
    f=h5py.File("confronti.h5","w")
    g0=f.require_group("D0" )
    g0["off"]  = offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix
    g0["back"] = back_ixsobj.offDiaDataSets[0].alignedSignalMatrix
    g0["diff"] =     offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix - 1.0*back_ixsobj.offDiaDataSets[0].alignedSignalMatrix
    
    g1=f.require_group("D1" )
    g1["off"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
    g1["back"] = back_ixsobj.offDiaDataSets[1].alignedSignalMatrix
    g1["diff"] =     offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix - 1.0*back_ixsobj.offDiaDataSets[1].alignedSignalMatrix
    
    g2=f.require_group("D2" )
    g2["off"]  = offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix
    g2["back"] = back_ixsobj.offDiaDataSets[2].alignedSignalMatrix
    g2["diff"] =     offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix - 1.0*back_ixsobj.offDiaDataSets[2].alignedSignalMatrix
    f.close()
    
    sys.exit(0)


print(" LAST PART ")
    
#######################################
# subtract background from off-diagonal
#######################################

scale = 1.0
offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[0].alignedSignalMatrix*scale

scale = 1.0
offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[1].alignedSignalMatrix*scale

scale = 1.0
offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[2].alignedSignalMatrix*scale

#######################################
# window
#######################################



f=h5py.File("confronti_L23.h5","w")
goff = f.require_group("off" )
g0=goff.require_group("prima_sub" )
g0["signal_prima"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix

subtract = np.zeros_like( offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix) 

##
#### SUBTRACT L23 PRE-EDGE TO OFF
##

# try subtraction the SiL23-pre-edge region to zero OFF-dia
for ii in range(len(offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[0,:])):
        inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[1].energy) >= 8.015,np.array(offdia_ixsobj.offDiaDataSets[1].energy) <= 8.022))[0]
        back = np.polyval(np.polyfit(np.array(offdia_ixsobj.offDiaDataSets[1].energy)[inds],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,ii],0),np.array(offdia_ixsobj.offDiaDataSets[1].energy))
        subtract[inds,ii]  = back[inds]
        offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[:,ii] -=back

g0["subtract"]     = subtract
g0["signal_dopo"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g0["energy"]       = offdia_ixsobj.offDiaDataSets[1].energy



##
#### SUBTRACT L23 PRE-EDGE TO DIA
##

gdia = f.require_group("dia" )
g0=gdia.require_group("prima_sub" )
g0["signal_prima"]  = dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix

subtract = np.zeros_like( offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix) 

# try subtraction the SiL23-pre-edge region to zero DIA
for ii in range(len(dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[0,:])):
        inds = np.where(np.logical_and(np.array(dia_ixsobj.offDiaDataSets[1].energy) >= 8.015,np.array(dia_ixsobj.offDiaDataSets[1].energy) <= 8.022))[0]
        back = np.polyval(np.polyfit(np.array(dia_ixsobj.offDiaDataSets[1].energy)[inds],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,ii],0),np.array(dia_ixsobj.offDiaDataSets[1].energy))
        subtract[inds,ii]  = back[inds]
        dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[:,ii] -=back

g0["subtract"]     = subtract
g0["signal_dopo"]  = dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g0["energy"]       = dia_ixsobj.offDiaDataSets[1].energy


saveDictH5( {"energy"    :offdia_ixsobj.offDiaDataSets[1].energy ,
             "angles_rad":offdia_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0,
             "signals":offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix   }  , "Signals111.h5" ) 

saveDictH5( {"energy"    :dia_ixsobj.offDiaDataSets[1].energy ,
             "angles_rad":dia_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0,
             "signals":dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix   }  , "Signals111_dia.h5" ) 

f.close()

sys.exit(0)




