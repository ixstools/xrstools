numpy
mpi4py
PyMca5 >=5.1
h5py
PyYAML
cython
fabio
scipy
matplotlib
numexpr
silx
six
sklearn
