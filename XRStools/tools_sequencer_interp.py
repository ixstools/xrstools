import numpy as np
import h5py
import glob
import json
import os
import h5py
import math

BATCH_PARALLELISM = 1


import os


def process_input(s, go=0, exploit_slurm_mpi = 0, stop_omp = False):
    open("input_tmp_%d.par"%go, "w").write(s)
    background_activator = ""
    if (go % BATCH_PARALLELISM ):
        background_activator = "&"

    prefix=""
    if stop_omp:
        prefix = prefix +"export OMP_NUM_THREADS=1 ;"
        
    if (  exploit_slurm_mpi==0  ):
        os.system(prefix +"mpirun -n 1 XRS_swissknife  input_tmp_%d.par  %s"%(go, background_activator))
    elif (  exploit_slurm_mpi>0  ):
        os.system(prefix + "mpirun XRS_swissknife  input_tmp_%d.par  %s"%(go, background_activator) )
    else:
        os.system(prefix + "mpirun -n %d XRS_swissknife  input_tmp_%d.par  %s"%(abs( exploit_slurm_mpi  ), go, background_activator) )
 


def select_rois( datadir = None,  roi_scan_num=None, roi_target_path = None, filter_path = None):

    if np.isscalar(roi_scan_num):
        scans = [roi_scan_num]
    else:
        scans = list(roi_scan_num)
        
    input_string = """
    create_rois: 
        expdata : {expdata} 
        scans : {scans} 
        roiaddress : {roi_target_path}
        filter_path : {filter_path}
    """.format(
        expdata = os.path.join( datadir, "hydra"),
        scans = scans,
        roi_target_path = roi_target_path,
        filter_path = filter_path
    )
    process_input( input_string,  exploit_slurm_mpi = 0 )
    

def get_reference(   roi_path =  None,
                     datadir = None,
                     reference_scan_list = None,
                     monitor_column = None,
                     extracted_reference_target_file = None,
                     isolate_spot_by = None
):
    signal_path = extracted_reference_target_file + ":/"

    input_string = """
    loadscan_2Dimages :
       expdata : {expdata} 
       roiaddress :  {roi_path} 
       monitorcolumn : {monitor_column} 
       scan_list : {reference_scan_list}  
       signaladdress : "{extracted_reference_target_file}:/references"
       isolateSpot : {isolate_spot_by}
       save_also_roi : True
  
       sumto1D  : 0
       energycolumn : 'stx'
    """
    s=input_string.format(
        expdata = os.path.join( datadir, "hydra"),
        reference_scan_list = reference_scan_list,
        monitor_column = monitor_column,
        roi_path = roi_path,
        isolate_spot_by = isolate_spot_by,
        signal_path = signal_path,
        extracted_reference_target_file = extracted_reference_target_file
    )
    process_input( s , exploit_slurm_mpi = 0) 

    
def extract_sample_givenrois(
        roi_path = None,
        datadir = None, 
        Start = None,
        End   = None,
        Thickness = None ,
        monitor_column = None,
        signals_target_file = None,
):
    for start in range(Start,End, Thickness):
        
        end = start+Thickness

        signal_path = signals_target_file + ":/_{start}_{end}".format(start=start, end=end)
        
        input_string = """
        loadscan_2Dimages :
            expdata : {expdata}
            roiaddress : {roi_path} 
            scan_interval : [{start}, {end}] 
            energycolumn : sty 
            signaladdress : {signal_path}
            monitorcolumn : {monitor_column}
            sumto1D  : 0
        """.format(
            expdata = os.path.join( datadir, "hydra"),
            roi_path = roi_path,
            start = start,
            end = end,
            monitor_column = monitor_column,
            signal_path = signal_path
        ) 
        process_input(input_string, exploit_slurm_mpi = 1)
        


        
def interpolate( peaks_shifts, interp_file_str,  interp_file_target_str):

    interp_file = h5py.File(  interp_file_str ,"r+")
    interp_file_target = h5py.File( interp_file_target_str   ,"r+")

    

    volum_list = list(interp_file.keys())
    scan_num_list = np.array([ int( t.split("_") [1]) for t in volum_list])
    ene_list      = np.array([    interp_file[vn]["scans"]["Scan%03d"%sn ]["motorDict"]["energy"].value for vn,sn in zip(volum_list, scan_num_list   )   ])

    print ( " ecco la scannumlist " , scan_num_list)
    print (" ecco ene_list", ene_list)

    order = np.argsort(  ene_list    )

    ene_list  = ene_list [order]
    scan_num_list  = scan_num_list [order]
    volum_list  = [ volum_list [ii]  for ii in order  ] 

    # raise
    for t_vn, t_sn, t_ene in list(zip(volum_list,  scan_num_list, ene_list    ))[0:]:
        rois_coeffs={}
        for roi_num, de in enumerate(     peaks_shifts   ):
            print ( roi_num, "===== " , t_ene+de , ene_list .min() , t_ene+de , ene_list .max()  ) 
            if  t_ene+de < ene_list .min() or t_ene+de > ene_list .max():
                continue
            
            print ( " CONTINUO ", t_ene+de, ene_list .min() ,ene_list .max() )

            i0 = np.searchsorted(   ene_list    , t_ene+de )-1
            assert(i0>=0)
            i1=i0+1
            print (i0, i1, len(ene_list))
            print (ene_list) 
            assert(i1<len( ene_list ))

            DE = (  ene_list[i1] -  ene_list[i0]   )
            df = (  t_ene+de  -  ene_list[i0]   )/ DE

            rois_coeffs[ roi_num  ] =  [   i0,(1-df)   , i1,df         ]
        print ( " for reinterpolation of ", t_vn ," interpolation scheme is the following ",  rois_coeffs)


        fscans = interp_file[ t_vn   ]["scans"]
        keys_list = list(  fscans.keys() )
        print ( " keylist ", keys_list)

        # interp_file_target.flush()

        fscans = interp_file_target[ t_vn   ]["scans"]
        keys_list = list(  fscans.keys() )
        print ( " keylist ", keys_list)

        print ( " roislist   keylist", list(rois_coeffs.keys())  )
        for k in keys_list:
            if k[:3]=="ROI":
                if int(k[3:]) not in rois_coeffs:
                    print (" rimuovo ", k)
                    del fscans[k]
        for sn in range(t_sn, t_sn+1):
            fScan = fscans["Scan%03d"% sn]
            keys_list = list(  fScan.keys() )
            for k in keys_list:
                if k!="motorDict":
                    if int(k) not in rois_coeffs:
                        print (" rimuovo da scans", k)
                        del fScan[k]


        for sn in range(t_sn, t_sn+1):
            fScan = fscans["Scan%03d"% sn]
            keys_list = list(  fScan.keys() )
            for k in keys_list:
                if k!="motorDict":
                    assert( int(k)  in rois_coeffs)
                    k = int(k)
                    i0,f0,i1,f1 = rois_coeffs[k]

                    matrix0 = interp_file[volum_list[i0]  ]["scans"]["Scan%03d"%( scan_num_list[i0]+sn-t_sn)  ][str(k)]["matrix"][:]
                    matrix1 = interp_file[volum_list[i1]  ]["scans"]["Scan%03d"%( scan_num_list[i1]+sn-t_sn)  ][str(k)]["matrix"][:]
                    monitor = np.ones( matrix0.shape[0],"f" )
                    newmatrix = f0* matrix0+f1*matrix1

                    if "matrix" in fScan[str(k)] :
                        del fScan[str(k)]["matrix"]
                    if "monitor" in fScan[str(k)] :
                        del fScan[str(k)]["monitor"]
                    if "monitor_divider" in fScan[str(k)] :
                        del fScan[str(k)]["monitor_divider"]

                    fScan[str(k)]["matrix"] = newmatrix
                    fScan[str(k)]["monitor"] = monitor
                    fScan[str(k)]["monitor_divider"] = 1.0

        

def synthetise_response(scan_address=None, target_address=None,             response_fit_options = None
):
    input_string = """
    superR_fit_responses :
       foil_scan_address : "{scan_address}"
       nref : 7                 # the number of subdivision per pixel dimension used to 
                                # represent the optical response function at higher resolution
       niter_optical  :  {niter_optical}    # the number of iterations used in the optimisation of the optical
                                # response
       beta_optical  :  {beta_optical}     # The L1 norm factor in the regularisation 
                                #  term for the optical functions
       pixel_dim : 1            # The pixel response function is represented with a 
                                #  pixel_dim**2 array
       niter_pixel : 10        # The number of iterations in the pixel response optimisation
                                # phase. A negative number stands for ISTA, positive for FISTA
       beta_pixel  :  0.0    # L1 factor for the pixel response regularisation

       ## The used trajectories are always written whith the calculated response 
       ## They can be reloaded and used as initialization(and freezed with do_refine_trajectory : 0 )
       ## Uncomment the following line if you want to reload a set of trajectories
       ## without this options trajectories are initialised from the spots drifts
       ##
       #   reload_trajectories_file : "response.h5"

       filter_rois : 0


       ######
       ## The method first find an estimation of the foil scan trajectory on each roi
       ## then, based on this, obtain a fit of the optical response function
       ## assuming a flat pixel response. Finally the pixel response is optimised
       ##
       ## There is a final phase where a global optimisation
       ## is done in niter_global steps.
       ##
       ## Each step is composed of optical response fit, followed by a pixel response fit.
       ## If do_refine_trajectory is different from zero, the trajectory is reoptimised at each step
       ## 
       niter_global  :  {niter_global}

       ## if do_refine_trajectory=1 the start and end point of the trajectory are free
       ##  if =2 then the start and end point are forced to a trajectory which is obtained
       ##  from a reference scan : the foil scan may be short, then one can use the scan of
       ##   an object to get another one : key *trajectory_reference_scan_address*
       ##

       do_refine_trajectory : 1

       ## optional: only if do_refine_trajectory = 2

       trajectory_reference_scansequence_address : "demo_newrois.h5:/ROI_FOIL/images/scans/"
       trajectory_threshold   : 0.1

       ## if the pixel response function is forced to be symmetrical 

       simmetrizza : 1

       ## where the found responses are written

       target_file : {target_address}
       # target_file : "fitted_responses.h5"

    """ 
    s=input_string.format(  scan_address=scan_address ,
                           target_address=target_address,
                           niter_optical = response_fit_options[ "niter_optical"],
                           beta_optical=response_fit_options["beta_optical"],
                           niter_global=response_fit_options["niter_global"]
    )
    process_input( s , exploit_slurm_mpi = 1, stop_omp = True) 


def  resynthetise_scan(
        old_scan_address= None,
        response_file  =  None ,
        target_address =  None,
        original_roi_path = None,
        resynth_z_square = None
):

    input_string = """
     superR_recreate_rois :
     ### we have calculated the responses in responsefilename
         ### and we want to enlarge the scan  by a margin of 3 times
         ### the original scan on the right and on the left 
         ###  ( so for a total of a 7 expansion factor )

         responsefilename :  {response_file}
         nex : 0

         ## the old scan covered by the old rois
         old_scan_address : {old_scan_address}

         ## where new rois and bnew scan are written
         target_filename : {target_address}
         filter_rois      : 0

         original_roi_path : {original_roi_path}

         resynth_z_square : {resynth_z_square}
"""
    s=input_string.format(  response_file = response_file ,
                            target_address = target_address,
                            old_scan_address=old_scan_address,
                            original_roi_path = original_roi_path +"/rois_definition",
                            resynth_z_square = resynth_z_square
    )
    process_input( s , exploit_slurm_mpi = 0, stop_omp = True) 



    
def get_scalars(  Start = None,  Thickness = None ,  
                  reference_address = None,
                  signals_file =  None, 
                  target_file = None,
                  
                  use_optional_solution=False,
                  save_factors = False,
                  load_factors_from = None,
                  selected_rois = None,
                  scal_prod_use_optional_solution= False,
                  scal_prod_load_factors         = False,
                  scal_prod_load_factors_from    = None,
):
    
    input_string = """
    superR_scal_deltaXimages :
       sample_address : {signals_file}:/_{start}_{end}/scans
       delta_address : {reference_address}


       # roi_keys       :  [60, 64, 35, 69, 34, 24, 5, 6, 71, 70, 39, 58, 56, 33]
       roi_keys       :  {selected_rois}

       nbin : 1
       target_address : {target_file}:/_{start}_{end}/scal_prods
    """
    if scal_prod_use_optional_solution:
        input_string = input_string+"""
       optional_solution : {target_file}:/_{start}_{end}/volume
        """

    if True:
        input_string = input_string+"""
       save_factors_on : factors_{start}_{end}.json
        """
    if scal_prod_load_factors :
        input_string = input_string+"""
       load_factors_from : %s
        """ % scal_prod_load_factors_from


    input_string = input_string .format(start=Start,
                                      end=Start+Thickness  ,
                                      signals_file = signals_file ,
                                      reference_address = reference_address, 
                                      target_file  = target_file,
                                      selected_rois = list(selected_rois)
    )
        
    process_input( input_string, exploit_slurm_mpi = 0)    
   

def get_volume(  Start = None,
                 Thickness = None,
                 volumes_file= None,
                 niter =  None,
                 beta  = None,
                 

):
    inputstring = """    
     superR_getVolume :
        scalprods_address : {volumes_file}:/_{start}_{end}/scal_prods
        target_address :  {volumes_file}:/_{start}_{end}/volume
        niter : {niter}
        beta : {beta}
        eps : 2e-07
        debin : [1, 1]
    """
    s=inputstring.format(start=Start, end=Start+Thickness,   
                         volumes_file = volumes_file, niter = niter, beta = beta )
    process_input(s, exploit_slurm_mpi = 0  )



    
def collect_factors(pattern="factors_*_*", newfile="newfactors.json"):

    files = glob.glob(pattern)
    indexes = [ int(s.split("_")[1]) for s in files ]
    order = np.argsort(indexes)
    files = [files[i] for i in order]
    print( files)
    files  = files[1:-1]
    result  = {}
    result2 = {}
    Nkeys = None
    for f in files:
        factors = json.load(open(f,"r"))
        if Nkeys is None:
            Nkeys = len(list(factors.keys()))
        assert(Nkeys == len(list(factors.keys())) )
        for k,val in factors.items():
            if k not in result:
                result[k] = 0.0
                result2[k] = 0.0
            result[k] += factors[k]/ len(files)
            result2[k] += (factors[k]*factors[k])/ len(files)
    json.dump(result,open(newfile,"w") )     
    keys = list(result.keys() )
    keys.sort(key=int)
    for k  in keys:
        print( k , " ",   result[k] , " " , math.sqrt( result2[k] - result[k]*result[k]    ) / result[k]  )

    
    
def tools_sequencer(  peaks_shifts = None,
                      datadir = None,
                      filter_path = None, 
                      roi_scan_num = None,
                      roi_target_path       = None,

                      first_scan_num =  None ,
                      Ydim        =  None , # not used 
                      Zdim        =  None ,
                      Edim        =  None ,
                      
                      monitor_column = None, 
                      signals_target_file = None,
                      interpolated_signals_target_file = None,
                                            
                      steps_to_do = None,

                      #####################################################
                      ## can be left to None, will be set to the used target
                      roi_path              = None,
                      signals_file = None,
                      interpolated_signals_file = None,

                      reference_clip = None,
                      isolate_spot_by = None,
                      reference_scan_list = None,
                      extracted_reference_target_file = None ,
                      response_target_file = None,
                      response_fit_options = None,
                      resynthetised_reference_and_roi_target_file  = None,
                      resynth_z_square = None,
                      
                      
                      selected_rois = None, 
                      scal_prod_use_optional_solution= False,
                      scalar_products_and_volume_target_file     = None,

                      volume_retrieval_beta  = None,
                      volume_retrieval_niter = None 
) :

    if roi_path is None:
        roi_path = roi_target_path        
    if signals_file  is None:
       signals_file   =  signals_target_file
    if interpolated_signals_file  is None:
       interpolated_signals_file   =  interpolated_signals_target_file
    
    if(steps_to_do["do_step_make_roi"]):   # ROI selection and reference scan
        select_rois(datadir         = datadir ,
                    roi_scan_num    = roi_scan_num ,
                    roi_target_path = roi_target_path,
                    filter_path     = filter_path
        )
                        
    if(steps_to_do["do_step_sample_extraction"]): 
        extract_sample_givenrois(
            roi_path = roi_path,
            datadir = datadir, 
            Start =  first_scan_num ,
            End   = (first_scan_num + Zdim * Edim ) ,
            Thickness = Zdim,
            monitor_column = monitor_column,
            signals_target_file = signals_target_file
        )
                
    if(steps_to_do["do_step_interpolation"]):    
        os.system("cp {signals_file} {interpolated_signals_target_file}".format(signals_file=signals_file, interpolated_signals_target_file =interpolated_signals_target_file) )
        interpolate(  peaks_shifts , signals_file ,  interpolated_signals_target_file )

        
    if(steps_to_do["do_step_extract_reference_scan"]):  # of course we need the REFERENCE SCAN

        get_reference( datadir = datadir,
                       roi_path = roi_path,
                       monitor_column = monitor_column,
                       extracted_reference_target_file = extracted_reference_target_file,
                       isolate_spot_by = isolate_spot_by,
                       reference_scan_list = reference_scan_list
        )        
        if reference_clip is not None:
            
            clip1, clip2= reference_clip
            
            ftarget = h5py.File( extracted_reference_target_file  ,"r+")
            for roi_scann in reference_scan_list:
                
                target_group = ftarget["references/scans/Scan%03d"% roi_scann ]
                
                for k in target_group.keys():
                    
                    if k != "motorDict":
                        print(" SHRINKING scan for ROI %s   in file roi_%d.h5 " %( k, roi_scann   ))
                        for dsn in ["matrix", "monitor", "xscale"]:
                            mat = target_group[k][dsn][()]
                            del target_group[k][dsn]
                            target_group[k][dsn] = mat[clip1:clip2]
            ftarget.close()            
        ftarget = h5py.File( extracted_reference_target_file  ,"r+")
        ftarget["references/scans/ScansSum"] = ftarget["references/scans/Scan%03d"% reference_scan_list[0] ]

        for other in reference_scan_list[1:]:
            source_group = ftarget["references/scans/Scan%03d"% other ]
            target_group = ftarget["references/scans/ScansSum"        ]
            
            for k in target_group.keys():
                if k != "motorDict":
                    print(" ADDING data for ROI %s   from file roi_%d.h5 " %( k, other   ))
                    mat = source_group[k]["matrix"][()]
                    target_group[k]["matrix"][:] +=  mat

        ftarget.close()

    if(steps_to_do["do_step_fit_reference_response"]):  
        synthetise_response(
            scan_address=  extracted_reference_target_file +":references/scans/ScansSum" ,
            target_address = response_target_file +":/FIT",
            response_fit_options = response_fit_options
        )

        
    if(steps_to_do["do_step_resynthetise_reference"]):  
        resynthetise_scan(
            old_scan_address=  extracted_reference_target_file +":references/scans/ScansSum" ,
            response_file  = response_target_file +":/FIT",
            target_address =  resynthetised_reference_and_roi_target_file +  ":/rois_and_reference",
            original_roi_path = roi_path,
            resynth_z_square = resynth_z_square
        )

        
    if(steps_to_do["do_step_scalars"]):          
        for start in range( first_scan_num , ( first_scan_num + Edim * Zdim ), Zdim ):            
            get_scalars( Start = start,  Thickness = Zdim , 
                         reference_address = resynthetised_reference_and_roi_target_file +  ":/rois_and_reference/scans/ScansSum" ,
                         signals_file  = interpolated_signals_file ,
                         target_file   = scalar_products_and_volume_target_file,
                         selected_rois = selected_rois                , 
                         scal_prod_use_optional_solution= scal_prod_use_optional_solution,
                         scal_prod_load_factors         = False,
                         scal_prod_load_factors_from    = None
            )

            
        if(scal_prod_use_optional_solution):
            collect_factors(pattern="factors_*_*.json", newfile="newfactors.json")
        
            for start in range( first_scan_num , ( first_scan_num + Edim * Zdim ), Zdim ):            
                get_scalars( Start = start,  Thickness = Zdim , 
                             reference_address = resynthetised_reference_and_roi_target_file +  ":/rois_and_reference/scans/ScansSum" ,
                             signals_file  = interpolated_signals_file ,
                             target_file   = scalar_products_and_volume_target_file,
                             selected_rois = selected_rois                , 
                             scal_prod_use_optional_solution= False,
                             scal_prod_load_factors         = False,
                             scal_prod_load_factors_from    = "newfactors.json"
                )


            
    if(steps_to_do["do_step_volume_retrieval"]):          
        for start in range( first_scan_num , ( first_scan_num + Edim * Zdim ), Zdim ):            
            get_volume(  Start = start,
                         Thickness = Zdim,
                         volumes_file = scalar_products_and_volume_target_file,
                         beta  = volume_retrieval_beta ,
                         niter = volume_retrieval_niter,
            )

            
