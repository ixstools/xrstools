import os
import json
import h5py
import numpy as np



def runit( s , work_dir, filename ) :
    open(  os.path.join(work_dir, filename) ,"w" ).write(s)
    os.system( "cd %s; XRS_swissknife %s ; cd .." % (work_dir, filename)  )


def prepare(
        work_dir="preparation_run4_16",
        data = "/data/id20/inhouse/data/run4_16/run6_ihr/rixs" ,
        scan_for_roi= 66            ,

        do_roi = True,
        do_roi_sample = True, 

        reference_scan = 66 ,              # the scan used for the reference
        monitorcolumn = "izero",
        do_recentering=False,
        scan_for_sample_roi =  268  ,           # Not necessary is recentering is not needed. If not given the rois file is assumed (when needed if needed) to be already there
        beta_response = 0
) :
    if not os.path.exists(work_dir):
        os.makedirs(work_dir)
    assert (os.path.exists( work_dir) and os.path.isdir( work_dir)  ), "Cannot get work directory %s "%work_dir    
    assert (os.access(work_dir, os.W_OK | os.X_OK)), " directory %s is not writable"%work_dir 
            
    preparation_parameters = {
        "work_dir" : work_dir,
        "data" : data,
        "scan_for_roi" : scan_for_roi, 
        "reference_scan" : reference_scan,
        "monitorcolumn" : monitorcolumn,
        "do_recentering" : do_recentering,
        "scan_for_sample_roi" : scan_for_sample_roi   
    }

    json.dump(preparation_parameters, open(os.path.join(work_dir, "preparation_parameters.json"),"w")  )


    if do_roi:
        s="""
        create_rois:
          expdata : "{data_file}" 
          scans : [{scan_for_roi}] 
          roiaddress : "roi.h5:/ROI_AS_SELECTED" 
        """.format( data_file = data,  scan_for_roi = scan_for_roi )

        runit( s , work_dir, "01_create_roi.yaml" )
        

    if do_recentering and do_roi_sample:
        s="""
        create_rois:
          expdata : "{data_file}" 
          scans : [{scan_for_roi} ] 
          roiaddress : roi_sample.h5:/ROI_AS_SELECTED 
        """.format( data_file = data,  scan_for_roi = scan_for_sample_roi )

        runit( s , work_dir, "02_create_roi_sample.yaml" ) 

        
    s="""
    loadscan_2Dimages :
      expdata :  "{data_file}" 
      roiaddress : roi.h5:/ROI_AS_SELECTED 
      scan_interval : [{reference_scan},{reference_scan_P1}] 
      signaladdress : calibration_scan 
      sumto1D  : 0
      energycolumn : 'Anal Energy'
      monitorcolumn : {monitorcolumn}
      save_also_roi : 1
    """.format( data_file = data,  reference_scan = reference_scan , reference_scan_P1 = reference_scan +1 , monitorcolumn = monitorcolumn )
    runit( s , work_dir, "03_load_scan_reference.yaml" ) 




    if do_recentering : 
        s="""
        loadscan_2Dimages :
          expdata :  "{data_file}" 
          roiaddress : roi_sample.h5:/ROI_AS_SELECTED 
          scan_interval : [{scan_for_roi},{scan_for_roi_P1}] 
          signaladdress : uncentered_sample_scan 
          sumto1D  : 0
          energycolumn : 'Anal Energy'
          monitorcolumn : {monitorcolumn}
          save_also_roi : 1
        """.format( data_file = data,  scan_for_roi = scan_for_sample_roi , scan_for_roi_P1 = scan_for_sample_roi +1 , monitorcolumn = monitorcolumn )
        runit( s , work_dir, "04_load_scan_sample.yaml" ) 


        s="""
        calculate_recenterings :
           bariA : roi_sample.h5:/ROI_AS_SELECTED/uncentered_sample_scan/scans/Scan%03d/ 
           bariB : roi.h5:/ROI_AS_SELECTED/calibration_scan/scans/Scan%03d/ 
           target : recentering.h5:/recentering
        """%( scan_for_sample_roi,  reference_scan )
        runit( s , work_dir, "05_setup_recentering.yaml" ) 
        

        s="""
        loadscan_2Dimages :
          expdata : {data_file}
          roiaddress : roi.h5:/ROI_AS_SELECTED 
          scan_interval : [{scan_for_roi},{scan_for_roi_P1}] 
          monitorcolumn : {monitorcolumn}
          recenterings  : recentering.h5:/recentering
          recenterings_confirmed : recentering.h5:/confirmed_shift 
          signaladdress : Centered_sample_scans 

          sumto1D  : 0
          energycolumn : 'Anal Energy'
        """.format( data_file = data,  scan_for_roi = scan_for_sample_roi , scan_for_roi_P1 = scan_for_sample_roi +1 , monitorcolumn = monitorcolumn )
        runit( s , work_dir, "06_calculate_recentering.yaml" ) 

        s="""
        superR_fit_responses:
           foil_scan_address : roi.h5:/ROI_AS_SELECTED/calibration_scan/scans/Scan%03d
           nref : 2 
           niter_optical : 50 
           beta_optical : %e
           niter_global : 2 
           pixel_dim  : 1 
           niter_pixel : 0 
           beta_pixel :  0 
           do_refine_trajectory  : 1 
           simmetrizza : 1
           filter_rois : 0
           target_file : fitted_response.h5:/ScanFittedResponse_%03d
           fit_lines   : 1 
        """%( reference_scan, beta_response, reference_scan ) 
        runit( s , work_dir, "07_fit.yaml" ) 


        s="""
        superR_recreate_rois :
           responsefilename : fitted_response.h5:/ScanFittedResponse_%03d
           nex : 0 
           old_scan_address : roi.h5:/ROI_AS_SELECTED/calibration_scan/scans/Scan%03d
           filter_rois : 0
           recenterings_refined : recentering.h5:/confirmed_shift
           target_filename : newscan.h5:/ROI_AS_SELECTED/calibration_scan/scans/ScanReSynth_%03d
        """%( reference_scan,  reference_scan,  reference_scan ) 
        runit( s , work_dir, "08_resynth.yaml" ) 



def extract(
        work_dir="preparation_run4_16",
        scan_interval = [130, 132] ,
        target_file = "SPECTRA.h5",
        temporary_file = "temporary.h5",
        niter = 0,
        niterLip = 20,
        beta = 0,    
        
) :
    assert (os.path.exists( work_dir) and os.path.isdir( work_dir)  ), "Cannot get work directory %s "%work_dir    
    assert (os.access(work_dir, os.W_OK | os.X_OK)), " directory %s is not writable"%work_dir 

    preparation_parameters = json.load( open(os.path.join(work_dir, "preparation_parameters.json"),"r")  )

    data = preparation_parameters["data"]
    reference_scan = preparation_parameters["reference_scan"]
    monitorcolumn = preparation_parameters["monitorcolumn"]
    scan_for_roi = preparation_parameters["scan_for_roi"]

    start =  scan_interval[0]
    end   = scan_interval[1]
    
    s="""
    loadscan_2Dimages :
      expdata : {data_file}
      roiaddress : newscan.h5:/ROI_AS_SELECTED/calibration_scan/scans/ScanReSynth_{reference_scan:03d}
      scan_interval : [{start}, {end}]
      signaladdress : ../{temporary_file}:/data_{start}_{end} 
      sumto1D  : 0
      energycolumn : 'Anal Energy'
      monitorcolumn : {monitorcolumn}
    """.format(data_file = data,  reference_scan = reference_scan ,  monitorcolumn = monitorcolumn,
               start = start, end = end, temporary_file=temporary_file)
    runit( s , work_dir, "31_harvest.yaml" ) 
    

    s="""
    extract_spectra :
     reference_address : newscan.h5:/ROI_AS_SELECTED/calibration_scan/scans/ScanReSynth_{reference_scan:03d}
     sample_address : ../{temporary_file}:/data_{start}_{end} 
     roiaddress :     newscan.h5:/ROI_AS_SELECTED/calibration_scan/scans/ScanReSynth_{reference_scan:03d}
     reference_scan : {reference_scan}
     scan_interval : [{start}, {end}]
     zmargin : 0
     target : ../{result_file}:/fromscans_{start}_{end}
     # final_plot : PLOT
     DE : 0.0
     niterLip : {niterLip}
     niter : {niter}
     beta :   {beta}
    """.format(reference_scan=reference_scan, start = start, end = end , niterLip = niterLip, niter = niter, beta = beta, result_file = target_file, temporary_file=temporary_file)
    runit( s , work_dir, "32_extract_spectra.yaml" ) 
