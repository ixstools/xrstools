## Only for Si so far

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from ab2tds import dabax 
import numpy as np

LAMBDAEV = dabax.LAMBDAEV

def Arb_Rot(angle=None, rot_axis=None):
    assert(len(rot_axis)==3)
    x,y,z = rot_axis
    rot_mat = np.array([[ 1 + (1-np.cos(angle))*(x*x-1) ,
                         -z*np.sin(angle)+(1-np.cos(angle))*x*y, 
                          y*np.sin(angle)+(1-np.cos(angle))*x*z ],
                          
                        [ z*np.sin(angle)+(1-np.cos(angle))*x*y ,
                          1 + (1-np.cos(angle))*(y*y-1),
                         -x*np.sin(angle)+(1-np.cos(angle))*y*z ],
                        
                        [-y*np.sin(angle)+(1-np.cos(angle))*x*z,
                          x*np.sin(angle)+(1-np.cos(angle))*y*z,
                          1 + (1-np.cos(angle))*(z*z-1) ]])
    return rot_mat


def Struct( hkl ) :
    center = np.array([1.0,1,1])/8.0
    ps = np.array([ [0,0,0.0], [1,1,0], [1,0,1], [0,1,1] ] )*0.5
    ps = np.concatenate(  [  ps -center, ps+ center ]  ) 
    res = 0
    for p in ps:
        res = res + np.exp( 2.0j * np.pi * np.dot( hkl, p )   ) 
    return res
    


def  Refl( E0,  FH, FmH, F0, nn, BH, T0, beam_direction=None ): ## BH est in Angstroems **-1 senza 2*pi
  
  k0 = 1/(LAMBDAEV/E0)
  
  K0_e = beam_direction*k0

  b_inv = 1.0 + np.dot( nn, BH     ) /np.dot( nn, K0_e    )

  b     = 1.0/b_inv

  # print(" ########################### ")
  # print(  K0_e  ) 
  # print(  BH    ) 
  # print(    np.linalg.norm(BH) ) 
  # print(" ########################### ")
  
  
  alpha = 1.0*(  np.dot( BH, BH+2*K0_e ) ) / k0/k0

  #######
  # risoluzione della formula
  q = b * FH*FmH
  z = (1-b)*F0/2.0 + b*alpha/2.0

  delta0_a = 1.0/2.0*(F0 -z + np.sqrt(  q+z*z  ) ) 
  delta0_b = 1.0/2.0*(F0 -z - np.sqrt(  q+z*z  ) )

  ##
  # rapporto fra DH e D0
  X_a  = (  2* delta0_a - F0     )/FmH
  X_b  = (  2* delta0_b - F0     )/FmH
  
  # gamma
  gamma = np.dot(K0_e, nn ) / np.linalg.norm(K0_e)

  ### c1,c2

  psi_a = 2*np.pi * k0  *delta0_a / gamma
  psi_b = 2*np.pi * k0  *delta0_b / gamma

  # print(" PSIs ", psi_a, psi_b)

  if 0 : 
    c_a = np.exp( - 1.0j *psi_a * T0)
    c_b = np.exp( - 1.0j *psi_b * T0)
    
  if psi_a.imag>b.imag:
    # A_ratio e' dato da X_b
    mu = -2*psi_b.imag
    c_a = 1.0
    c_b = 0.0
  else:
    # A_ratio e' dato da X_a
    mu = -2*psi_a.imag
    c_a = 0.0
    c_b = 1.0

  A_ratio = ( X_a*X_b*(c_a-c_b) )/(c_b*X_b -c_a*X_a )

  I_ratio  = (A_ratio*(A_ratio.conjugate())).real

  return I_ratio, A_ratio, mu

def DoubleCrystalMonoSpectra( E0 =  7950.0,
                              hkl = np.array(  [ 1,1,1  ]),
                              Dene =  [0.0],
                              NP = 1000,
                              DeltaAngle = 0.0 ):
     
    Table_f0   = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
    Table_f1f2 = dabax.Dabax_f1f2_Table("f1f2_Windt.dat")
    Sif0  = Table_f0.Element("Siva")
    Sif1f2= Table_f1f2.Element("Si")
    
    d_Si = 5.431
    
    lamd0 =  LAMBDAEV / E0
    
    Ghkl = np.array( hkl,"d")
    G = 1.0/d_Si *np.linalg.norm(Ghkl)
    
    sint = lamd0*G /2.0
    Brad  = np.arcsin(sint)

    Drad_Dev = -1/np.sqrt(1-sint*sint )  * G /2.0 * LAMBDAEV / E0/E0

    
    Brad2 = Brad+DeltaAngle*np.pi/180.0
    
    print(" ANGOLO t , t2, ", Brad * 180.0 / np.pi   , Brad2 * 180.0 / np.pi, DeltaAngle)

    Sh = Struct( hkl)
    S0 = Struct( [0,0,0]) 
    
    anomalous_part = Sif1f2.f1f2Energy(  E0  )
    
    f0_H = Sif0.f0Energy(E0, Brad)
    f0_0 = Sif0.f0Energy(E0, 0)
    
    
    FH = Sh*(  f0_H +  anomalous_part    )
    F0 = S0*(  f0_0 +  anomalous_part    )
    
    CCmsec  = 2.99792458e+10
    mel_g    = 9.1093837e-28
    ee_stat  = 4.803204e-10
    
    Vcell = 1.0e-24 * (d_Si)**3
    
    omega0 = E0 * 2*np.pi *CCmsec / LAMBDAEV *1.0e+8
    
    Fconv_F2f = -4.0*np.pi*ee_stat*ee_stat / mel_g / omega0/omega0 / Vcell
    
    ## Per la simmetria centrale attorno al punto 1/8 1/8 1/8  non mi curo delle differenza fra Fh e F di meno h
    ## mi basta Fh
    
    F0 = Fconv_F2f* F0
    FH = Fconv_F2f* FH
    
    ## prendiamo BH lungo Y
    
    BH = np.array([0.0,0,G])
    
    
    nn =  np.array(  [ 0.0, 0,  -1.0]  )   # niente asimmetria
    
    res = []
    
    beam_direction   = np.array( [ -np.cos(Brad ), 0, -np.sin(Brad ) ]      )
    beam_direction_2 = np.array( [ -np.cos(Brad2), 0, -np.sin(Brad2) ]      )

    
    for offset in Dene :
        E = E0+offset
        I_ratio , A_ratio, mu  = Refl( E, FH, FH, F0, nn, BH  ,  1.0e8, beam_direction = beam_direction   )  ## BH est in Angstrom**-1
        I_ratio2, A_ratio, mu  = Refl( E, FH, FH, F0, nn, BH  ,  1.0e8, beam_direction = beam_direction_2 )  ## BH est in Angstrom**-1
        res.append( [E, I_ratio* I_ratio2  ] )
    return np.array(res)    , Drad_Dev



def CoefficientsReflection(E0  = 7950.0, Eused = 7950.0, hkl = [1,1,1], Drad = [0.0] ,  asymm_grad=0.0, tthv_grad=10.0, tth_grad=0.0, psi_grad=0.0):
  
  Table_f0   = dabax.Dabax_f0_Table("f0_WaasKirf.dat")
  Table_f1f2 = dabax.Dabax_f1f2_Table("f1f2_Windt.dat")
  Sif0  = Table_f0.Element("Si")
  Sif1f2= Table_f1f2.Element("Si")

  d_Si = 5.431

  lamd0 =  LAMBDAEV / E0

  Ghkl = np.array( hkl,"d")
  G = 1.0/d_Si *np.linalg.norm(Ghkl)  # the norm of the scattering vector is 1/d_S

  sint = lamd0*G /2.0
  Brad = np.arcsin(sint)

  print(" NOMINAL BRAGG ANGLE ", Brad * 180.0 / np.pi )

  Sh = Struct( hkl)
  S0 = Struct( [0,0,0]) 

  anomalous_part = Sif1f2.f1f2Energy(  E0  )

  f0_H = Sif0.f0Energy(E0, Brad)
  f0_0 = Sif0.f0Energy(E0, 0)

  print( " Supposed ratio rho_G/rho  for 111 ",    (Sh*f0_H).real/    (S0*f0_0).real    )

  
  FH = Sh*(  f0_H +  anomalous_part    )
  F0 = S0*(  f0_0 +  anomalous_part    )

  print( " FH and F0 " , FH, F0)
  
  CCmsec  = 2.99792458e+10
  mel_g    = 9.1093837e-28
  ee_stat  = 4.803204e-10

  Vcell = 1.0e-24 * (d_Si)**3

  omega0 = E0 * 2*np.pi *CCmsec / LAMBDAEV *1.0e+8

  Fconv_F2f = -4.0*np.pi*ee_stat*ee_stat / mel_g / omega0/omega0 / Vcell

  ## Per la simmetria centrale attorno al punto 1/8 1/8 1/8  non mi curo delle differenza fra Fh e F di meno h
  ## mi basta Fh

  F0 = Fconv_F2f* F0
  FH = Fconv_F2f* FH

  ## prendiamo BH lungo Y

  asymm_angle = asymm_grad * np.pi/180.0

  ##################################################################################################################
  # questo per estrarre mu_norm2
  #
  BH_simple = np.array(  [  0.0, 0,   G ]  )
  nn_simple =   np.array(  [  0.0, 0,  -1 ]  )

  beam_direction = np.array( [   0,  0,  -1.0 ]      )

  
  dum1, dum2, mu_norm2  = Refl( E0,  FH, FH, F0, nn_simple, BH_simple ,  1.0e8,     beam_direction = beam_direction   ) ## BH est in Angstrom**-1
  ###################################################################################################################
  
  # CALCOLO DEL COSENO FRA NN E DIREZIONE DETECTOR
  #  prima dimensione orrizontale lungo X, seconda verticale lungo Y, terza verso l'osservatore della pagina, lungo z

  #
  # DECT_VERSOR nel sistema del laboratorio
  #
  tthv = tthv_grad *np.pi/180.0
  tth  = tth_grad  *np.pi/180.0
  
  dect_versor =  np.array([   -np.cos(tthv)  *np.cos(tth)    ,
                              +(-1)*np.cos(tthv)  *np.sin(tth)    ,  #### qua forse errore di segno
                              +np.sin(tthv)
  ]  )
  
  res = []
  GEO0 = None
  for offset in Drad :
      
    thetarad = Brad+offset

    #
    # 
    #
    G0_versor    = np.array([ np.sin(thetarad),
                              0,
                              np.cos(thetarad)
    ])

    norm_versor_0  = (-1)*np.array([ np.sin(thetarad -asymm_angle),  ## (-1) because we are pointing toward the interior of the sample
                                     0,
                                     np.cos(thetarad -asymm_angle)
    ])


    BH = G * G0_versor
        

    norm_versor = np.dot(
        Arb_Rot( angle = psi_grad*np.pi/180.0,
                 rot_axis  = G0_versor  ),
        norm_versor_0
    ) 



    
    beam_direction = np.array( [ -1.0, 0, 0  ]      )
    beam_direction_out = beam_direction -2*G0_versor*  ((      G0_versor*beam_direction ).sum())
    cos_in  =  abs( ( beam_direction     * norm_versor ).sum()    )
    cos_out =  abs( ( beam_direction_out * norm_versor ).sum()    )



    print (BH)
    print ( beam_direction_out  / lamd0   -  beam_direction  / lamd0  )
    print (lamd0)

    U = np.dot(BH,BH)/3.0
    DK = ( dect_versor - beam_direction ) / lamd0
    DKr = DK - BH
    DKr = ( dect_versor - beam_direction_out ) / lamd0

    print( " U ", np.dot(BH,BH)/3.0, "   Ldirect ", (np.dot(DK,DK)/U) , 2.25 ,   " Lreverse     ", (np.dot(DKr,DKr)/U) , 2.25 , "    Lcross      ",  (np.dot(DKr,DK)/U) , 3.0/4.0  ) 
    

    I_ratio, A_ratio, mu  = Refl( Eused,  FH, FH, F0, norm_versor, BH ,  1.0e8,  beam_direction = beam_direction ) 

    cos_dect_norm = abs( np.dot(dect_versor, norm_versor) ) 

    print( " COSCOS ",np.dot(dect_versor, norm_versor)  ) 
    
    geo =  1.0/(mu + mu_norm2/cos_dect_norm)

    if GEO0 is None:
        GEO0 = geo
    
    res.append( [
                  I_ratio*cos_out/cos_in,
                  geo/GEO0,
                  geo*(A_ratio.real)*2.0/GEO0,
                  geo* abs(A_ratio.real*A_ratio.real +  A_ratio.imag*A_ratio.imag  )/GEO0])
  return np.array(res).T



def calculate_RC_factors(E0 = None , hkl=None, hkl_mono=None,  asymm_grad = None,
                         tthv_grad=None, tth_grad=None, psi_grad= None,
                         source_distance = None, magnification=None, DeltaAngle = 0 ,
                         nEpoints = 1000, nApoints= 1000, nHpoints = 100,
                         Ew_source_ev = None, Aw_rocking_rad=None,
                         Hw_slit_mm = None):

    """
     This routine perform the calculation of the averaged coefficients considering different wavelenghts
     and different incoming directions.
     Contribution from different energies are considered being mutually incoherent.
     Moreover ech incoming direction is considered incoherent.
     This is a strong assumption because the beam in reality has a coherence lenght.
     But here we calculate the weight of all the contributions by a sort of ray-tracing, the weight being obtained
     from dynamical theory for a plane wave of a given energy and of a given direction calculating the reflection
     of the monochromators and what happens in the sample:
     A sampling is done over the energy, considering and energy width Ew_source_ev around nominal E0.
     A sampling is done also over the wavefront height. Each height define and incoming angle
     ( given by the source position and the distance from it).

     ** E0 is the nominal energy, eV

     ** source_distance is given in meters

     ** asymm_grad is a parameter of the sample cut

     **  tthv_grad, tth_grad, psi_grad are used to calculate the absorption coefficient of the way out to the detector after inelastic scattering

     ** magnification : converts the  ray angular deviation from the central ray before the monochromator+mirror to the angular deviation after.

     ** Delta angle is zero for two parallel monochromator crystals

     ** Ew_source_ev, Aw_rocking_rad, Hw_slit_mm gives the widths of the samplings.
            Hw_slit_mm should be given and correspond the the height of the beam.
            Ew_source_ev should encompass the rocking curve arounf the nominal E0, and should encompass also the shift due to refraction index
            Aw_rocking curve also shoudl be large enough.
    ** The number of sampling points should be large enough for he sampling to be fine. So if Ew_source_ev and other widths are taken too large 
       you might end up needing too large numbers of points. 
    """
    
    Ew = Ew_source_ev
    Aw = Aw_rocking_rad 
    Hw = Hw_slit_mm
    
    if hkl_mono is None:
        hkl_mono = hkl

        
    DEpoints = np.linspace( - Ew/2.0 + Ew/nEpoints/2.0   ,
                            + Ew/2.0 - Ew/nEpoints/2.0   ,
                            num = nEpoints,
                            endpoint= True)
    Drads_points = np.linspace(  - Aw/2.0 + Aw/nApoints/2.0   ,
                          + Aw/2.0 - Aw/nApoints/2.0   ,
                          num=nApoints , endpoint= True)
    dHpoints = np.linspace(  - Hw/2.0 + Hw/nHpoints/2.0   ,
                             + Hw/2.0 - Hw/nHpoints/2.0   ,
                             num=nHpoints , endpoint= True)

    print( "=================== " , DeltaAngle )
    ##########################################################################################
    ## Drad_dEv is the derivative of the Bragg angle ( from the surface, i.e. 0 is grazing)
    ## versus the energy in ev. It is a negative number
    ##
    monochromator_response_energy, Drad_dEv = DoubleCrystalMonoSpectra( E0 = E0 ,
                                                                              hkl =   hkl_mono ,
                                                                              Dene =  DEpoints,
                                                                              DeltaAngle =  DeltaAngle )  


    np.savetxt("monochromator_response_energy.txt",monochromator_response_energy)
    
    ref_curve_angle = 0.0
    A_curve_angle = 0.0
    B_curve_angle = 0.0
    C_curve_angle = 0.0

    ##########################################################################################
    ## calculations are done at different offset from theta Bragg given by Drads_points :
    ##  drads are added to theta
    #
    #  We calculate this curve at one energy : Eused
    # and for different grazing angles
    ref_0, A_0,C_0,B_0  = CoefficientsReflection(E0 = E0 ,
                                                 Eused = E0 +0.0,
                                                 hkl = hkl ,
                                                 Drad = Drads_points ,
                                                 asymm_grad = asymm_grad,
                                                 tthv_grad = tthv_grad  ,
                                                 tth_grad = tth_grad,
                                                 psi_grad = psi_grad
    )
    
    np.savetxt("sample_ref.txt",np.array( [Drads_points, ref_0] ).T )
    
    totsomma = 0.0
    
    for dh in dHpoints :
        print(" dh ", dh)
                                                      ##################################################################################################
        shiftA  = dh*1e-3 / source_distance           # dHpoints is given in millimiter this is the ray angle from the central ray in radians
        shiftA2 =  shiftA / magnification             # After reflections by the the monochromator and by the mirrors, this is the angular deviation
                                                      # of the considered ray from the central ray

        assert( Drad_dEv < 0.0 ) 

        ######################################################################################
        #
        # The first monochromator mirror is considered facing downward. So imagine that shiftA is positive.
        # This means that ( considereing an horizontal central ray) our ray is pointing to the ceiling,
        # so this means an increase in the theta grasing angle. The center of the response curve shits by shiftE  
        # and goes to lower energies (Drad_dEv is negative). The response of the crystal moves by +shiftE
        
        shiftE   = shiftA  / Drad_dEv   # shift dell energia di Bragg. Immaginiamo un shiftA positivo. Tutta la curva si sposta ad energie piu basse 

        for Ene, Beam in  monochromator_response_energy:    ## We interate over the reflectivity curve.

            totsomma += Beam  # Beam is the reflectivity
            
            Ene = Ene + shiftE    # the considered point of the reflectivity curve has moved by shiftE


            ###################################################################################################################
            ##
            ## Now on the side of the sample. As for the monochromator we have calculated the contributions of the sample
            ## for one fixed energy and different angles. We are hitting the sample with a deviation shiftA2 of the grazing angle
            ## from the nominal Bragg angle of the central ray at the nominal energy  E0.
            ## We need to read the contribution for a given deviation from the bragg angle. 
            ## Because the Bragg angle increase by (Ene-E0) *Drad_dEv
            ## then the deviation from the considered  ray and this Bragg angle decreases by such term

            
            myshiftA = shiftA2  - (Ene-E0) *Drad_dEv  ## se l'energia e' piu grande allora (Ene-E0) *Drad_dEv e' negativo
                                                      ## Contemporaneamente se l'energia e' piu' alta incontro il massimo a piu' bassi angoli
                                                      ##  Quindi devo aggiungere agli angoli di calcolo un termine positivo in modo che il massimo venga letto ad un angolo piu basso


            ref = np.interp(   Drads_points+myshiftA , Drads_points , ref_0   ) 
            A   = np.interp(   Drads_points+myshiftA , Drads_points , A_0     ) 
            B   = np.interp(   Drads_points+myshiftA , Drads_points , B_0     ) 
            C   = np.interp(   Drads_points+myshiftA , Drads_points , C_0     ) 

            
            ref_curve_angle = ref_curve_angle + Beam * ref
            A_curve_angle   = A_curve_angle   + Beam * A
            B_curve_angle   = B_curve_angle   + Beam * B
            C_curve_angle   = C_curve_angle   + Beam * C

    rocking_curve = np.array([  Drads_points, ref_curve_angle/totsomma ] )     
    A_curve = np.array([  Drads_points, A_curve_angle/totsomma ] )     
    B_curve = np.array([  Drads_points, B_curve_angle/totsomma ] )     
    C_curve = np.array([  Drads_points, C_curve_angle/totsomma ] )     

    np.savetxt("ref.txt",(rocking_curve).T )
    
    return { "RC_curve": rocking_curve, "A_curve": A_curve, "B_curve": B_curve, "C_curve": C_curve }
