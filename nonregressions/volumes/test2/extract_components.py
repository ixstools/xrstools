from h5py import *
from sklearn.decomposition import NMF
from pylab import *
import json


NC = 4
model = NMF(n_components=NC, init='random', random_state=0, max_iter = 200)

d=File("solution_all_spectra.h5","r") ["data"][()]

d.shape = d.shape[0],-1


W = model.fit_transform(d.T)

for i in range(NC):
    plot(model.components_[i])
show()




File("components.h5","w")["components"] = model.components_



interpolation_infos_file = "interpolation_infos.json"

info_dict={}
for i in range(NC):
    dizio = {}
    info_dict[str(i)] = {"coefficients":dizio}
    c = model.components_[i]
    np = len(c)
    for j in range(np):
        dizio[str(j)] = float(c[j])

json.dump(info_dict,open( interpolation_infos_file,"w"), indent=4)
