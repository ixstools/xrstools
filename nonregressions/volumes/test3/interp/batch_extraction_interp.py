import numpy as np
import h5py
import glob
import json
import os
import h5py
import math

from XRStools import tools_sequencer_interp_galaxies
from XRStools import xrs_read,  xrs_rois

import os

def main():

    os.system("mkdir results")

    
    peaks_shifts = np.array(
                     [  0.0,
                        0.0,
                        0.0,
                        0.0,
                     ]) - 0.0

    dataprefix = "/data/raffaela/" 
    datadir =  dataprefix + "GS9_dataGalaxies/"
    filter_path =  dataprefix + "mymask.h5:/mymask"
    filename_template = "GS9_%05d_01"
    data_path_template =  datadir + filename_template + ".nxs:/root_spyc_config1d_RIXS_00001/scan_data/data_07"
    reference_data_path_template =  dataprefix + "kapton_%05d_01" + ".nxs:/root_spyc_config1d_RIXS_00001/scan_data/data_07"

    
    monitor_path_template = None
    # monitor_path_template = datadir + filename_template +"_monitor.hd5:/monitor"

    print( datadir +(filename_template%1)+".nxs" ) 
    energy_exp_grid  = h5py.File( datadir +(filename_template%1)+".nxs" ,"r")["/root_spyc_config1d_RIXS_00001/scan_data/actuator_1_1"][()]
    
    
    first_scan   =  1

    Ydim = 16
    Zdim = 16
    Edim = 19

    reference_scan_list =  [1]
    
    roi_scan_num = list(range(1,3))

    reference_clip = None
    reference_clip = [ 0, 200 ]

    ## in the reference scan for each position there is a spot with a maximum. We set zero the background which is further than
    ## such radius from the maximum
    isolate_spot_by = 7

    #### For the fit of the response function based on reference scans
    response_fit_options = dict( [
        ["niter_optical" , 40],
        ["beta_optical"  , 0.1],
        ["niter_global"  , 1  ]
    ])
    resynth_z_square = 0
    selected_rois = list(range(0,4) )

    scal_prod_use_optional_solution = True

    volume_retrieval_beta  = 6.0e-1
    volume_retrieval_niter = 100 

    
    steps_to_do = {
        "do_step_make_roi":                      False,
        "do_step_sample_extraction":             False,
        "do_step_interpolation":                 False,
    
        "do_step_extract_reference_scan":        False,
        "do_step_fit_reference_response":        False,
        "do_step_resynthetise_reference":        False,
        
        "do_step_scalar":               False, 
        "do_step_volume_retrieval"      :    False,
        "do_step_put_all_in_one_stack"  : True
    }

    scalar_products_and_volume_target_file  = "results/scalar_prods_and_volume.h5"

    roi_target_path          = "results/myrois.h5:/ROIS"
    reference_target_file    = "results/response.h5"
    signals_target_file      = "results/extracted.h5" 
    interpolated_signals_target_file = "results/interpolated_signals.h5"

    extracted_reference_target_file = "results/reference.h5"
    response_target_file = "results/response.h5"


    resynthetised_reference_and_roi_target_file = "results/resynthetised_roi_and_scan.h5"
    

    
    tools_sequencer_interp_galaxies.tools_sequencer(  peaks_shifts          = peaks_shifts          ,
                      data_path_template    = data_path_template    ,
                      filter_path           = filter_path           ,
                      roi_scan_num          = roi_scan_num          ,
                      roi_target_path       = roi_target_path       ,
                      
                      first_scan         = first_scan         ,                      
                      Ydim                  = Ydim                  ,
                      Zdim                  = Zdim                  ,
                      Edim                  = Edim                  ,
                      
                      monitor_path_template = monitor_path_template ,
                      signals_target_file = signals_target_file,
                      interpolated_signals_target_file = interpolated_signals_target_file,
                      
                      steps_to_do = steps_to_do,

                      
                      reference_clip = reference_clip,
                      isolate_spot_by =  isolate_spot_by,
                      reference_scan_list = reference_scan_list,
                      reference_data_path_template = reference_data_path_template,
                      extracted_reference_target_file    = extracted_reference_target_file   ,
                      response_target_file = response_target_file,
                      response_fit_options = response_fit_options,
                      resynthetised_reference_and_roi_target_file = resynthetised_reference_and_roi_target_file,
                      resynth_z_square = resynth_z_square,
                      
                      selected_rois = selected_rois,
                      scal_prod_use_optional_solution = scal_prod_use_optional_solution ,
                      scalar_products_and_volume_target_file      = scalar_products_and_volume_target_file ,

                      volume_retrieval_beta  = volume_retrieval_beta ,
                      volume_retrieval_niter = volume_retrieval_niter ,
                      energy_exp_grid = energy_exp_grid
    ) 

    if steps_to_do["do_step_put_all_in_one_stack"] :

        volumefile  = scalar_products_and_volume_target_file

        h5file_root = h5py.File( volumefile ,"r+" )

        scankeys = list( h5file_root.keys())
        scankeys.sort()
        volumes = []
        for k in scankeys:
            if k[:1]!="E":
                continue
            print( k)
            if "volume" in h5file_root[k]:
                volumes.append( h5file_root[k]["volume"]  )
        # volume = np.concatenate(volumes,axis=0)
        volume = np.array(volumes)
        h5py.File("concatenated_volume.h5","w")["volume"] = volume
        h5file_root.close()
            




main()
