import numpy as np
import h5py
import glob
import json
import os
import h5py
import math

from XRStools import tools_sequencer_interp
from XRStools import tools_sequencer_esynth
from XRStools import xrs_read,  xrs_rois

import os
def main():
    filter_path = None
    # filter_path = "mask.h5:/FILTER_MASK/filter"

    roi_scan_num   = list(range(592,600))
    reference_scan_list = list(range(592,600))
    
    monitor_column = "izero/0.000001"
    
    first_scan_num = 464
    
    Ydim        = None   # not used, it is the scan lenght , it is given by the data, contained in the scan
    Zdim        = 2
    Edim        = 62

    elastic_scan_for_peaks_shifts = 38

    datadir =   "/data/id20/inhouse/data/run5_17/run7_ihr/"

    reference_clip = None

    isolate_spot_by = 7

    response_fit_options = dict( [
        ["niter_optical" , 40],
        ["beta_optical"  , 0.1],
        ["niter_global"  , 1  ]
    ])

    resynth_z_square = 0

    selected_rois = list(range(0,24)) + list( range(36,60) ) 

    scal_prod_use_optional_solution = False

    volume_retrieval_beta  = 0.12
    volume_retrieval_niter = 100 
        
    steps_to_do = {
        "do_step_make_roi":                      True,
        "do_step_sample_extraction":             False,
        "do_step_interpolation":                 False,
        "do_step_extract_reference_scan":             False,
                
        "do_step_fit_reference_response":               False,
        "do_step_resynthetise_reference":               False,
        "do_step_scalars"               :    False,
        "do_step_volume_retrieval"      :    False,
        "do_step_put_all_in_one_stack"  : False
    }

    
    os.system("mkdir results")


    scalar_products_and_volume_target_file  = "results/scalar_products_and_volume.h5"

    roi_target_path    = "results/myrois.h5:/ROIS"
        
    signals_target_file = "results/signals.h5"
    
    interpolated_signals_target_file = "results/interpolated_signals.h5"

    extracted_reference_target_file = "results/reference.h5"
    
    response_target_file = "results/response.h5"

    resynthetised_reference_and_roi_target_file = "results/resynthetised_roi_and_scan.h5"

    ###########################################################################################
    ######  LOADING PEAKS SHIFTS
    ###### peaks_shifts = h5py.File("../peaks_positions_for_analysers.h5","r")["peaks_positions"][()]
    ###### assert( len(peaks_shifts) == 72)

    if steps_to_do["do_step_interpolation"]:
        roiob = xrs_rois.roi_object()
        roiob.loadH5( roi_target_path  )
        elastic = xrs_read.Hydra( datadir )
        elastic.set_roiObj( roiob )
        elastic.get_compensation_factor( elastic_scan_for_peaks_shifts , method='sum')
        el_dict = elastic.cenom_dict
        Enominal = np.median(  list( el_dict.values() ) )
        peaks_shifts = np.array([    el_dict["ROI%02d"%i] if ("ROI%02d"%i) in  el_dict else nan    for i in range( 72)  ] )
        peaks_shifts-= Enominal
    else:
        peaks_shifts = None
    
    ##############################################################
    ##########################################################################
    
    tools_sequencer_interp.tools_sequencer(  peaks_shifts          = peaks_shifts          ,
                      datadir               = datadir               ,
                      filter_path           = filter_path           ,
                      roi_scan_num          = roi_scan_num          ,
                      roi_target_path       = roi_target_path       ,
                      
                      steps_to_do = steps_to_do,
                      
                      first_scan_num =  first_scan_num,
                      Ydim        =  Ydim       ,                    # not used 
                      Zdim        =  Zdim       ,
                      Edim        =  Edim       ,

                      monitor_column = monitor_column,
                      signals_target_file = signals_target_file,
                      interpolated_signals_target_file = interpolated_signals_target_file,

                      reference_scan_list = reference_scan_list,
                      reference_clip = reference_clip,
                      extracted_reference_target_file = extracted_reference_target_file ,
                      isolate_spot_by =  isolate_spot_by,
                      response_target_file = response_target_file,
                      response_fit_options = response_fit_options,
                      
                      resynthetised_reference_and_roi_target_file = resynthetised_reference_and_roi_target_file,
                                             resynth_z_square = resynth_z_square,
                                             
                      selected_rois = selected_rois,
                      
                      scal_prod_use_optional_solution = scal_prod_use_optional_solution ,
                      scalar_products_and_volume_target_file      = scalar_products_and_volume_target_file ,

                      volume_retrieval_beta  = volume_retrieval_beta ,
                      volume_retrieval_niter = volume_retrieval_niter 
                      
    )

    if steps_to_do["do_step_put_all_in_one_stack"] :

        volumefile  = scalar_products_and_volume_target_file

        h5file_root = h5py.File( volumefile ,"r+" )

        scankeys = list( h5file_root.keys())
        scankeys.sort()
        volumes = []
        for k in scankeys:
            if k[:1]!="_":
                continue
            print( k)
            if "volume" in h5file_root[k]:
                volumes.append( h5file_root[k]["volume"]  )
        # volume = np.concatenate(volumes,axis=0)
        volume = np.array(volumes)
        h5py.File("concatenated_volume.h5","w")["volume"] = volume
        h5file_root.close()

    

            
            
main()


