import glob
import os
import h5py
import numpy as np

if(1):
    inputs = glob.glob("0*yaml")
    inputs.sort()
    for fn in inputs:
        if  fn != "01_create_rois_galaxies.yaml":
            res = os.system("mpirun -n 1 XRS_swissknife %s" % fn )
            assert(res==0)

    templates_list = [ open("06_superR_scalarProducts.yaml_template").read(),
                       open("07_superR_getVolume.yaml_template")     .read()
    ]
    for iE in range(16) :

        for template in templates_list :
            s = template.format(iE=iE)
            open("input.par","w").write(s)
            res = os.system("mpirun -n 1 XRS_swissknife input.par"  ) 
            assert(res==0)


f = h5py.File("results/volumes.h5", "r")         
stack = []
for i in range(16):
    stack.append( f["VOL_E%d/Volume"%i][:,:,45:65]  )
h5py.File("results/volumes_all_energies.h5","w")["volume"] = np.array(stack)

ref_stack = h5py.File("/data/scisofttmp/mirone/raffaela/volumes_all_energies_reference.h5","r")["volume"][()]


assert   (np.abs( ref_stack- stack  ).max()<1.0 ) , "difference between stack and reference stack too big" 


save_path="results"
if os.environ["CLEAN_RESULTS"] == "true" :
    os.system("rm %s/*" % save_path)

