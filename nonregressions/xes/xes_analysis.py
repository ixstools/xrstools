import os
import json
import h5py
import numpy as np

import XRStools. XES_spectra_extraction as  spectra_extraction
if(1):

    os.system("xz -d --keep  preparation_run4_16/ROI.xz ; mv  preparation_run4_16/ROI   preparation_run4_16/roi.h5" ) 
    os.system("xz -d --keep  preparation_run4_16/roi_sample.h5.xz" ) 

    if 1:
        spectra_extraction.prepare(
        work_dir="preparation_run4_16",
        data = "/data/id20/inhouse/data/run4_16/run6_ihr/rixs" ,
        scan_for_roi= 66            ,           
        
        reference_scan = 66 ,              # the scan used for the reference
        monitorcolumn = "izero",
        
        scan_for_sample_roi =  268  ,   # Not necessary if recentering is not needed.
                                        # If not given the rois file is assumed (when needed if needed)
                                        # to be already there    
        do_roi = False,
        do_roi_sample = False,
        
        do_recentering= True,
        
        beta_response = 1.0
        ) 

spectra_extraction.extract(
    work_dir="preparation_run4_16",
    scan_interval = [130, 132] ,
    target_file = "SPECTRA.h5",
    temporary_file = "temporary.h5",
    #    niter = 50,
#    niterLip = 20,
#     beta = 1000000
)

f=h5py.File("SPECTRA.h5","r")

ex = f["/fromscans_130_132/2/energies_130"][()]
ey = f["/fromscans_130_132/2/spectraByLine_130"][()]

results = np.array( [ ex, ey ]  ).T 
np.savetxt(   "results/spectra.txt" , results  )

ref = np.loadtxt( "results/spectra_ref.txt") 

assert( abs(ref-results).max( )< 1.0e-7) 

if   "CLEAN_RESULTS" in os.environ and   os.environ["CLEAN_RESULTS"] == "true" :
    os.system("rm results/*")
