#include<stdlib.h>
#include <yaml-cpp/yaml.h>
#include <string>
#include<iostream>
#include<stdio.h>
#include <boost/filesystem.hpp>
#include<math.h>

#include <highfive/H5File.hpp>
#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>
#include <highfive/H5DataType.hpp>

#define assertm(exp, msg) assert(((void)msg, exp))

namespace h5 = HighFive;

struct DimPars_struct {
  size_t  NE  ;    
  size_t  NV  ;    
  size_t  NROI; 
  size_t  DIMZ; 
  size_t  DIMY; 
  size_t  DIMX;
  int  ZSTART ;
  int  ZEND ; 
};
typedef DimPars_struct  DimPars;

struct InputFileNames_struct {
  std::string DSname;
  std::string DDname;
  std::string SSname;
  std::string COEFFSname;
};
typedef  InputFileNames_struct InputFileNames;

float * read_volume(std::string fn, std::vector<size_t> &dims) {

  float * result; 
  std::string testh5_name(fn) ;
  h5::File test_file( testh5_name  , h5::File::ReadOnly);
  h5::DataSet test_dataset = test_file.getDataSet("data");
  dims = test_dataset.getDimensions();
  size_t tot = 1;
  for(size_t  i=0; i< dims.size(); i++) tot *= dims[i];
  result  =  (float*) malloc( tot *  sizeof(float));
  test_dataset.read( result , h5::AtomicType<float>()    ) ;
  
  return result;  
}

void save_volume(std::string fn, float * X , std::vector<size_t> dims) {

  std::string testh5_name(fn) ;
  h5::File test_file( testh5_name  , h5::File::ReadWrite | h5::File::Create | h5::File::Truncate );
  h5::DataSet dataset = test_file.createDataSet<float>("data",  h5::DataSpace(dims));
  dataset.write_raw(X);
}



class SolVol {
public:
  float *__restrict__ ptr;
};
  
class ProjectionVol {
public:
  float *__restrict__   ptr;
};

class CoefficientsVol {
public:
  float * __restrict__ ptr;
};

class FreeFactsVol {
public:
  float * __restrict__ ptr;
};

// DS SHAPE  (7, 72, 10, 41, 241)
//  DD SHAPE  (7, 72)
//  SS SHAPE  (7, 72, 241, 241)
class Problem {
public:
  Problem(InputFileNames &if_names_a, DimPars & dimpars_a ){
    this->if_names = if_names_a;
    this->dimpars  = dimpars_a;

    {
      std::vector<size_t> dims ; 
      this->DS = read_volume( if_names.DSname, dims) ;
      dimpars.NV   = dims[0] ; 
      dimpars.NROI = dims[1] ; 
      dimpars.DIMZ = dims[2] ; 
      dimpars.DIMY = dims[3] ; 
      dimpars.DIMX = dims[4] ;
    }
    
    
    if(dimpars.ZSTART>=0) {
      int dimz = 1+ dimpars.ZEND - dimpars.ZSTART ;
      float *ds = (float*) malloc(size_t(dimpars.NV)*size_t(dimpars.NROI)*size_t(dimz)* size_t(dimpars.DIMY)* size_t(dimpars.DIMX) *  sizeof(float) ) ;
      size_t BLOCK = size_t(dimpars.DIMZ)* size_t(dimpars.DIMY)* size_t(dimpars.DIMX  ) ;
      size_t block = size_t(        dimz)* size_t(dimpars.DIMY)* size_t(dimpars.DIMX  ) ;
      size_t offset  = size_t(dimpars.ZSTART)* size_t(dimpars.DIMY)* size_t(dimpars.DIMX) ;
      for(int iv=0; iv< (int) dimpars.NV; iv++) {
	for(int ir=0; ir< (int) dimpars.NROI; ir++) {
	  memcpy(
		 ds + (iv*dimpars.NROI+ir)*block    ,
		 DS + (iv*dimpars.NROI+ir)*BLOCK+offset,
		 block*sizeof(float)
		 );
	}
      }
      dimpars.DIMZ  = dimz ;
      free( DS ) ;
      this->DS = ds ;
    }
    
    {
      std::vector<size_t> dims ; 
      this->DD =  read_volume( if_names.DDname, dims) ;
      assert( dims[0]  == dimpars.NV   ) ;
      assert( dims[1]  == dimpars.NROI ) ;
    }

    {
      std::vector<size_t> dims ; 
      this->SS = read_volume( if_names.SSname, dims) ;
      assert( dims[0]  == dimpars.NROI ) ;
      assert( dims[1]  == dimpars.DIMX   ) ;
      assert( dims[2]  == dimpars.DIMX   ) ;      
    }
      
    {
      std::vector<size_t> dims ; 
      this->coefficients = read_volume( if_names.COEFFSname, dims) ;
      dimpars.NE = dims[0]  ;
      assert(      dims[1]  == dimpars.NV     ) ;
      assert(      dims[2]  == dimpars.NROI   ) ;
    }
    

    /*{
      int cnt=0;
      for(int iE=0; iE< dimpars.NE; iE++) {
	for(int iV=0; iV< dimpars.NV; iV++) {
	  for(int iR=0; iR< dimpars.NROI; iR++) {
	    this->coefficients[cnt] = ( iE==iV  );
	    if(iR!=1) 
	      this->coefficients[cnt] = 0;
	    cnt++;
	  }
	}
      }
    }
    */
    

    DimPars *d = & dimpars;
    

    this->Matff = (float*) malloc(   d->NE * d->NE * d->NROI * sizeof(float) ) ; 
    this->MatSffF2  = (float*) malloc(   d->NE*d->NE* d->DIMX*d->DIMX   *      sizeof(float) ) ;    
    this->setMatff();
    
    this->VectA = (float*) malloc(   d->NE *  d->DIMZ *  d->DIMY * d->DIMX * sizeof(float) ) ;


    this->MatR  = (float*) malloc(   d->NROI * sizeof(float) ) ;
    
    this->VectR  = (float*) malloc(   d->NROI * sizeof(float) ) ;
    
  }
  
#define SOL_addr( iE, iz, iy , ix  ) ((( (iE)*DIMZ + (iz))*DIMY+  (iy))*DIMX+  (ix))
#define SS_addr(  iroi, ix1, ix2  ) (( (iroi)*DIMX+  (ix1))*DIMX+  (ix2))
#define PRO_addr( iV, iroi, iz, iy,  ix  ) ((( (  (iV)*NROI        +iroi  )*DIMZ + (iz))*DIMY+  (iy))*DIMX+  (ix))

  void setVectA_and_Mat(SolVol target,   FreeFactsVol F ) {
    setMatSffF2(F) ;
    setVectA( target,    F ) ;
  };
  
  void setVectA(SolVol target,   FreeFactsVol F ) {
    int NE = dimpars.NE;
    int NV = dimpars.NV;
    int NROI = dimpars.NROI;
    int DIMZ = dimpars.DIMZ;
    int DIMY = dimpars.DIMY;
    int DIMX = dimpars.DIMX;
#pragma omp   parallel for    
    for(int ie=0 ; ie<NE; ie++) {	
      for( int iz=0; iz<DIMZ; iz++) {
	for( int iy=0; iy<DIMY; iy++) {
	  for( int ix=0; ix<DIMX; ix++) {
	    float tmp=0.0;
	    for(int iv=0 ; iv<NV; iv++) {	
	      for(int ir=0 ; ir<NROI; ir++) {	
		tmp +=    DS[  PRO_addr( iv,ir,  iz, iy,  ix        )  ] *  coefficients[ (ie*NV   +iv )*NROI +ir]   *F.ptr[ir] ; 
	      }
	    }
	    target.ptr[ SOL_addr( ie, iz, iy , ix  )   ]= tmp ; 
	  }
	}
      }
    }
  }
  void project_solution(SolVol target){
    int NE = dimpars.NE;
    int DIMZ = dimpars.DIMZ;
    int DIMY = dimpars.DIMY;
    int DIMX = dimpars.DIMX;
#pragma omp   parallel for    
    for(int ie=NE-1 ; ie<NE; ie++) {	
      for( int iz=0; iz<DIMZ; iz++) {
	for( int iy=0; iy<DIMY; iy++) {
	  for( int ix=0; ix<DIMX; ix++) {
	    target.ptr[ SOL_addr( ie, iz, iy , ix  )   ]= 0.6 ; 
	  }
	}
      }
    }
  }

  void setMatff() {
    int NE = dimpars.NE;
    int NV = dimpars.NV;
    int NROI = dimpars.NROI;
#pragma omp   parallel for    
    for(int ir=0; ir<NROI; ir++) {
      for(int ie1=0 ; ie1<NE; ie1++) {	
	for(int ie2=0 ; ie2<NE; ie2++) {
	  float tmp = 0;
	  for(int iv=0; iv<NV; iv++) {
	    tmp+=coefficients[ (ie1*NV   +iv )*NROI +ir]*coefficients[ (ie2*NV   +iv )*NROI +ir  ] ; 
	  }
	  Matff[    (ie1*NE +ie2 )*NROI  +  ir ] = tmp;
	}
      }
    }
  }
  
  
  void setMatSffF2(FreeFactsVol F) {
    int NE = dimpars.NE;
    int NROI = dimpars.NROI;
    int DIMX = dimpars.DIMX;
#pragma omp   parallel for    
    for(int ie1=0 ; ie1<NE; ie1++) {	
      for(int ie2=0 ; ie2<NE; ie2++) {
	for(int ix1=0 ; ix1<DIMX; ix1++) {	
	  for(int ix2=0 ; ix2<DIMX; ix2++) {
	    float tmp=0;
	    for(int ir=0; ir<NROI; ir++) {
	      tmp +=   F.ptr[ir]*F.ptr[ir] * Matff[    (ie1*NE +ie2 )*NROI  +  ir ] *     SS[  SS_addr (  ir, ix1, ix2  ) ]  ;

	    }
	    MatSffF2[  ((ie1*NE +ie2 )*DIMX + ix1)*DIMX + ix2] = tmp;
	  }
	}
      }
    }
  }
  
  void setFreeFacts(FreeFactsVol V , SolVol C) {
      float buff[100];
    {
      int NE = dimpars.NE;
      int NV = dimpars.NV;
      int NROI = dimpars.NROI;
      int DIMZ = dimpars.DIMZ;
      int DIMY = dimpars.DIMY;
      int DIMX = dimpars.DIMX;

      
      float *Ctmp = (float*)malloc(  NV*NROI*DIMZ*DIMY*DIMX               *sizeof(float));
      {
	
	memset(Ctmp,0,  NV*NROI*DIMZ*DIMY*DIMX               *sizeof(float));
	float tmp=0;
#pragma omp  parallel for      
	for(int iv=0 ; iv<NV; iv++) {
	  for(int ir=0 ; ir<NROI; ir++) {	
	    for( int iz=0; iz<DIMZ; iz++) {
	      for( int iy=0; iy<DIMY; iy++) {
		for(int ix=0 ; ix<DIMX; ix++) {
		  tmp=0.0;
		  for(int ie=0 ; ie<NE; ie++) {
		    tmp += C.ptr[SOL_addr( ie,iz,iy,ix   )] * coefficients[ (ie*NV   +iv )*NROI +ir] ; 
		  }
		  Ctmp[ ((((iv)*NROI+ir)*DIMZ+iz)*DIMY+iy)*DIMX+ix] = tmp;
		}
	      }
	    }
	  }
	}
      }
      
      for(int ir=0 ; ir<NROI; ir++) {
	double forza =0;
	float tmp = 0.0f ; 
	for( int iz=0; iz<DIMZ; iz++) {
	  for( int iy=0; iy<DIMY; iy++) {
	    
	    for(int iv=0 ; iv<NV; iv++) {

	      forza=0;
	      for(int ix1=0 ; ix1<DIMX; ix1++) {	
		for(int ix2=0 ; ix2<DIMX; ix2++) {
		  
		  tmp +=
		    Ctmp[ ((((iv)*NROI+ir)*DIMZ+iz)*DIMY+iy)*DIMX+ix1] *
		    Ctmp[ ((((iv)*NROI+ir)*DIMZ+iz)*DIMY+iy)*DIMX+ix2] *
		    SS[  SS_addr (  ir, ix1, ix2  ) ] ;
		  forza += SS[  SS_addr (  ir, ix1, ix2  ) ]; 
		}
	      }
	    }
	  }
	}
	buff[ir] = tmp ;
	printf(" FORZA%d %e\n", ir, forza);
      }
      free(Ctmp);
    }
    printf("1\n");
    {
      int NE = dimpars.NE;
      int NV = dimpars.NV;
      int NROI = dimpars.NROI;
      int DIMZ = dimpars.DIMZ;
      int DIMY = dimpars.DIMY;
      int DIMX = dimpars.DIMX;
      for(int ir=0 ; ir<NROI; ir++) {	
	float tmp = 0.0f ; 
	for(int ie=0 ; ie<NE; ie++) {
	  for(int iv=0 ; iv<NV; iv++) {	
	    for( int iz=0; iz<DIMZ; iz++) {
	      for( int iy=0; iy<DIMY; iy++) {
		for(int ix=0 ; ix<DIMX; ix++) {	
		  tmp +=   C.ptr[SOL_addr( ie,iz,iy,ix   )] *  DS[  PRO_addr (  iv, ir,iz,iy,ix ) ]* coefficients[ (ie*NV   +iv )*NROI +ir  ]  ;
		}
	      }
	    }
	  }
	}
	{
	  float sum=0;
	  {
	    for(int iv=0; iv<NV; iv++) {
	      sum+= DD[iv*NROI+ir];
	    }
	  }
	  float f = V.ptr[ir];
	  printf(" L'errore per %d sarebbe %e\n", ir,      f*f *buff[ir] - 2* tmp*f  + sum );
	  f = tmp/buff[ir];
	  printf(" L'errore dopo per %d sarebbe %e   %e %e %e\n", ir,      f*f *buff[ir] - 2* tmp*f  + sum , buff[ir], tmp, sum);
	}
	if( buff[ir]) {
	  V.ptr[ir] = tmp / buff[ir];
	} else {
	  V.ptr[ir] = 0 ; 
	}
	printf(" setto %d a %e\n", ir, V.ptr[ir]);
      }
    }
  }
  
  void applyMatA( SolVol target,SolVol source) {
    int NE = dimpars.NE;
    int DIMZ = dimpars.DIMZ;
    int DIMY = dimpars.DIMY;
    int DIMX = dimpars.DIMX;

#pragma omp  parallel for          
    for(int ie1=0 ; ie1<NE; ie1++) {	
      for(int iz=0; iz< DIMZ; iz++) {
	for(int iy=0; iy< DIMY; iy++) {
	  
	  for(int ix1=0; ix1< DIMX; ix1++) {
	    float tmp = 0.0 ; 
	    for(int ie2=0 ; ie2<NE; ie2++) {
	      for(int ix2=0; ix2< DIMX; ix2++) {
		tmp +=  source.ptr[SOL_addr( ie2, iz, iy , ix2  ) ]*MatSffF2[  ((ie1*NE +ie2 )*DIMX + ix1)*DIMX + ix2] ;

	      }
	    }
	    target.ptr[SOL_addr( ie1, iz, iy , ix1  ) ]= tmp ; 
	  }
	}
      }
    }
  };
  
  void save(SolVol X, std::string name) {
    std::vector<size_t> dims = {size_t(dimpars.NE),size_t(dimpars.DIMZ), size_t(dimpars.DIMY),  size_t(dimpars.DIMX) };
    save_volume( name,X.ptr, dims);
    
  };

  
  template<class T> size_t size() { return 0; };  
  



  void gradReg_2_Add(SolVol   target, SolVol   source , float betaE, float betaV) {
    int NE = dimpars.NE;
    int DIMZ = dimpars.DIMZ;
    int DIMY = dimpars.DIMY;
    int DIMX = dimpars.DIMX;

    if( betaE==0 && betaV ==0 ) return;
    
    for( int iE=0; iE<NE; iE++) {
      for( int iz=0; iz<DIMZ; iz++) {
	for( int iy=0; iy<DIMY; iy++) {
	  for( int ix=0; ix<DIMX; ix++) {
	    float add = 0.0 ; 
	    if(iE<NE-1) {
	      add +=    betaE*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE+1,iz,iy,ix   ) ]  )   ; 
	    }
	    if(iE>0) {
	      add +=    betaE*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE-1,iz,iy,ix   ) ]  )   ; 
	    }
	    if(iz<DIMZ-1) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz+1,iy,ix   ) ]  )   ; 
	    }
	    if(iz>0) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz-1,iy,ix   ) ]  )   ; 
	    }
	    if(iy<DIMY-1) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy+1,ix   ) ]  )   ; 
	    }
	    if(iy>0) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy-1,ix   ) ]  )   ; 
	    }
	    if(ix<DIMX-1) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy,ix+1   ) ]  )   ; 
	    }
	    if(ix>0) {
	      add +=    betaV*(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy,ix-1   ) ]  )   ; 
	    }
	    target.ptr[SOL_addr( iE,iz,iy,ix   )] += add; 
	  }
	}
      }
    }
  }


  void gradReg_1_Add(SolVol  target, SolVol  source , float betaE, float betaV) {
    int NE = dimpars.NE;
    int DIMZ = dimpars.DIMZ;
    int DIMY = dimpars.DIMY;
    int DIMX = dimpars.DIMX;

    if( betaE==0 && betaV ==0 ) return;
    
    for( int iE=0; iE<NE; iE++) {
      for( int iz=0; iz<DIMZ; iz++) {
	for( int iy=0; iy<DIMY; iy++) {
	  for( int ix=0; ix<DIMX; ix++) {
	    float add = 0.0 ; 
	    if(iE<NE-1) {
	      add +=   copysign( betaE,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE+1,iz,iy,ix   ) ]  )  ) ; 
	    }
	    if(iE>0) {
	      add +=   copysign( betaE,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE-1,iz,iy,ix   ) ]  )   ); 
	    }
	    if(iz<DIMZ-1) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz+1,iy,ix   ) ]  )   ); 
	    }
	    if(iz>0) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz-1,iy,ix   ) ]  )   ); 
	    }
	    if(iy<DIMY-1) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy+1,ix   ) ]  )   ); 
	    }
	    if(iy>0) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy-1,ix   ) ]  )   ); 
	    }
	    if(ix<DIMX-1) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy,ix+1   ) ]  )   ); 
	    }
	    if(ix>0) {
	      add +=   copysign( betaV,(  source.ptr[SOL_addr( iE,iz,iy,ix   )]-source.ptr[ SOL_addr( iE,iz,iy,ix-1   ) ]  )   ); 
	    }
	    target.ptr[SOL_addr( iE,iz,iy,ix   )] += add; 
	  }
	}
      }
    }
  }
  
  template <typename T>
  void allocate(T &vol) {
    vol.ptr  =  (float *) malloc(  size<T>() * sizeof(float) );
  };
  
  template <typename T>
  void settozero(T vol) {
    memset( vol.ptr, 0,  size<T>() * sizeof(float) );
  };

  template <typename T>
  void settoVal(T __restrict__ vol, float val) {
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      vol.ptr[i] = val ; 
    }
  }
  
  template <typename T>
  void copy(T __restrict__ target, T __restrict__ source ) {
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      target[i] = source[i] ; 
    }
  }

  template <typename T>
  void Scal( T  source , float alpha) {
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      source.ptr[i] = source.ptr[i] *alpha ; 
    }
  }

  template <typename T>
  void copyScal(T  target, T  source , float alpha) {
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      target.ptr[i] = source.ptr[i] *alpha ; 
    }
  }

  template <typename T>
  void copyScal(T  target, float *  source , float alpha) {
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      target.ptr[i] = source[i] *alpha ; 
    }
  }

  
  template <typename T>
  double  scalar(T  target, T  source ) {
    size_t numels =   this->size<T>();
    double res=0.0;

    for(size_t i = 0; i<numels; i++) {
      res += target.ptr[i] *source.ptr[i]  ; 
    }

    return res;
  }
  
  template <typename T>
  void AXPBYCR(float a , T  vol_a, float b, T  vol_b , float c, T __restrict__ vol_res ){
    size_t numels =   this->size<T>();
    for(size_t i = 0; i<numels; i++) {
      vol_res.ptr[i] = a*vol_a.ptr[i] + b*vol_b.ptr[i] +c*vol_res.ptr[i]; 
    }
  };
  
  DimPars dimpars;
  InputFileNames if_names ; 
  
  float * __restrict__ DS;
  float * __restrict__ DD;
  float * __restrict__ SS;
  
  float *coefficients ; 
  
  
  float *Matff     ;
  float *MatSffF2  ;
  float *VectA     ;
  float *MatR      ;
  float *VectR     ;
  
  
};

template<>  size_t Problem::size<SolVol> () {  return size_t(dimpars.NE)*size_t(dimpars.DIMZ)*size_t(dimpars.DIMY)*size_t(dimpars.DIMX) ;   };
template<>  size_t Problem::size<ProjectionVol>() {  return  size_t(dimpars.NV)*size_t(dimpars.NROI)*size_t(dimpars.DIMZ)* size_t(dimpars.DIMY)* size_t(dimpars.DIMX) ;  };
template<>  size_t Problem::size<FreeFactsVol>() {  return  size_t(dimpars.NROI) ;  };


int main(int argc, char ** argv)  {

  int index_input;
  {
    const char * usage= "frsv [-h] input_file   \n"
                        "   options :\n"
                        "        -h   prints this help \n"
                        "   arguments :\n"
                        "       input_file : a yaml file containing \n"
                        "         DSname : test0_DS.h5 \n"
                        "         DDname : test0_DD.h5 \n"
                        "         SSname : test0_SS.h5 \n"
                        "         COEFFSname : coefficients.h5 \n";
  
    int hflag = 0;
    // char *cvalue = NULL;
    int c;
    opterr = 0;
    //  while ((c = getopt (argc, argv, "hc:")) != -1) {
    while ((c = getopt (argc, argv, "h")) != -1) {
      switch (c)
	{
	case 'h':
	  hflag = 1;
	  break;
	  
	  // case 'c':
	  //   cvalue = optarg;
	  //   break;
	case '?':
	  if (optopt == 'c')
	    fprintf (stderr, "Option -%c requires an argument.\n", optopt);
	  else if (isprint (optopt))
	    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
	  else
	    fprintf (stderr,
		     "Unknown option character `\\x%x'.\n",
		     optopt);
	  return 1;
	default:
	  abort ();
	}
    }
    index_input = optind ; 
    if (hflag || index_input != (argc-1) ) {
      std::cout << " USAGE \n"  << usage ; 
    }
  }
    
  YAML::Node mockup_config;
  mockup_config = YAML::LoadFile(argv[index_input]);

  assert(  mockup_config["DSname"] ) ;
  assert(  mockup_config["DDname"] ) ;
  assert(  mockup_config["SSname"] ) ;
  assert(  mockup_config["COEFFSname"] ) ;

  DimPars dimpars ; 
  dimpars.ZSTART = -1; dimpars.ZEND = 100000 ; 
  if( mockup_config["ZSTART"] )  {
    dimpars.ZSTART =      mockup_config["ZSTART"].as<int>();
    dimpars.ZEND   =      mockup_config["ZEND"  ].as<int>();
  } ;



  float betaE = 0.0, betaV = 0.0 ;
  
  if( mockup_config["betaE"] )  {
    betaE = mockup_config["betaE"].as<float>();
  } ;
  if( mockup_config["betaV"] )  {
    betaV = mockup_config["betaV"].as<float>();
  } ;

  
  boost::filesystem::path p(argv[index_input]);
  std::string dirname = p.parent_path().string()+"/";

  
  InputFileNames if_names = {
    dirname + mockup_config["DSname"].as<std::string>() ,
    dirname + mockup_config["DDname"].as<std::string>() ,
    dirname + mockup_config["SSname"].as<std::string>(), 
    dirname + mockup_config["COEFFSname"].as<std::string>() 
  };

  Problem pb( if_names, dimpars );


  SolVol X, grad ,   XvectA, Xtmp;
  
  pb.allocate(X);
  pb.allocate(Xtmp);
  pb.allocate(grad);
  pb.allocate(XvectA);

  ProjectionVol Perror;
  
  pb.allocate(Perror);


  FreeFactsVol ffacts,fftmp;
  
  pb.allocate(ffacts);
  pb.allocate(fftmp);


  pb.settoVal(ffacts     ,1.0f);


  /*
 	ffacts.ptr[0]=0;
	  ffacts.ptr[2]=0;
	  ffacts.ptr[3]=0;
   */
  
  {
    double norma = sqrt(pb.scalar(ffacts, ffacts)); 
    pb.Scal( ffacts,1.0/norma ) ;
  }
  pb.settoVal(X     ,0.0f);
  
  for(int iter_c = 0; iter_c<10; iter_c++) {
    
    pb.setVectA_and_Mat( XvectA,   ffacts ) ;

    float Lip=1.0; 
    
    pb.settoVal(Xtmp     ,1.0f);

    for(int i=0; i< 10; i++) {
      if(0) {
	pb.copyScal(fftmp, ffacts, 1.0);
	pb.setFreeFacts( fftmp ,  X) ;
      }

      pb.applyMatA(grad , Xtmp);

      pb.gradReg_2_Add(grad, Xtmp , betaE, betaV) ;

      double norma = sqrt(pb.scalar(grad, grad  ));
      pb.copyScal( Xtmp, grad,1.0/norma ) ;
      printf("Norma Lipschitz %e\n", norma);
      Lip = norma*2;
    }


    for(int iter = 0; iter<40; iter++) {

      pb.settozero(grad);
      
      pb.applyMatA(grad , X);
      
      pb.gradReg_2_Add(grad, X , betaE, betaV ) ;
      
      pb.AXPBYCR( -1.0/Lip, grad,  +1.0/Lip, XvectA, 1.0,  X);

      
      // pb.project_solution(X);

      
      double merit =    pb.scalar(grad,  grad)  + pb.scalar( XvectA,  XvectA) - 2*pb.scalar(grad,  XvectA);      
      printf(" iter         %d %e\n", iter, merit);
      
    }
    pb.save(X, "solution.h5");
    
    pb.setFreeFacts( ffacts ,  X) ;
    
    {
      double norma = sqrt(pb.scalar(ffacts, ffacts)); 
      pb.Scal( ffacts,1.0/norma ) ;
    }
    printf(" set ok \n");
  }

  pb.save(X, "solution.h5");
  
}

