Installation
============

  * If you install from a Debian package you can skip the following points, install it , and then go directly to the code invocation section

  * Using Git, sources can be retrived with the following command ::

      git clone https://gitlab.esrf.fr/ixstools/xrstools

  * for a local installation you can use  ::

	python setup.py install --prefix=~/packages

    then to run the code you must do beforehand ::

	export PYTHONPATH=/home/yourname/packages/lib/python2.7/site-packages
	export PATH=/home/yourname/bin:$PATH


  * To install by creating a virtual environment ::

      export MYPREFIX=/REPLACE/WITH/YOUR/TARGET
      cd ${MYPREFIX}
      python3 -m venv myenv
      source ${MYPREFIX}/myenv/bin/activate
      pip install pip --upgrade
      pip install setuptools --upgrade

      git clone https://gitlab.esrf.fr/ixstools/xrstools

      cd ${MYPREFIX}/xrstools/
      pip install -r requirements.txt
      python setup.py install

  * Examples can be found in the nonregression directory. 


  * For the roi selection tool you need a recent version of pymca installed on your sistem


  * Usage examples can be found in the non regression directory.
         
	  
