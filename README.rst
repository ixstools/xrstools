XRStools
===========================================

Main development website: https://

|Build Status| |Appveyor Status|

Bla Bla 

References
----------

* reference

Installation
------------

With PIP
........

As most Python packages, pyFAI is available via PIP::

   pip install xrstools [--user]

Provide the *--user* to perform an installation local to your user.
Under UNIX, you may have to run the command via *sudo* to gain root access an
perform a system wide installation.



Documentation
-------------

Documentation can be build using this command and Sphinx (installed on your computer)::

    python setup.py build build_doc
